package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class AspectCategoryPolarityExtractor {


	//Map<Set<String>, String> map;
	//Set<String> aspectCategories;

	//List<String> aspectCategoryList;

	SWN3 sentiWordNet;
	HashMap<String,Double> categoryPolarityScores;
	String [] CATEGORIES = {"ACCOMMODATION","TRANSPORT","AMBIANCE","COST","CULTURE","ENTERTAINMENT","SCENARY"};

	public AspectCategoryPolarityExtractor(){
		this.sentiWordNet = new SWN3();
		this.categoryPolarityScores = new HashMap<String, Double>();

	}

	public  Map<String,Double> extractAspectCategoriesFromReview(TravelReview review,  LinearClassifier<String,String> lc, ColumnDataClassifier cdc,Annotation annotation,StanfordCoreNLP pipeline ) throws FileNotFoundException, ClassNotFoundException, IOException{

		Datum <String, String> datum = null;
		annotation = new Annotation(review.review);
		pipeline.annotate(annotation);
		List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
		Double reviewScore = 0.0;//score for whole review.

		for(CoreMap sentence:sentences){
			Double sentenceScore = 0.0;
			datum = cdc.makeDatumFromLine(sentence.toString() + "\t");
			String categoryStr = lc.classOf(datum); //predict aspect category
			List<String> catList = getCategoriesFromStr(categoryStr); // put predicted categories in a list
			for(CoreLabel word:sentence.get(TokensAnnotation.class)){ // tokenize sentence
				sentenceScore += getSentimentScore(word.lemma()); //get sentiment score for the whole sentence
			}

			for(String cat:catList){
				this.categoryPolarityScores.put(cat.trim(), this.categoryPolarityScores.get(cat.trim()) + sentenceScore );
			}
		}

		//normalize values
		for(String category:CATEGORIES){
			this.categoryPolarityScores.put(category.trim(), convertToStandardScore(this.categoryPolarityScores.get(category.trim())/sentences.size()) );
		}
		//end of normalize

		//		System.out.println(review.review);
		//		for (String cat: categoryPolarityScores.keySet()){
		//
		//			String key =cat.toString();
		//			Double value = categoryPolarityScores.get(cat);  
		//			System.out.println(key + " " + value);  
		//		} 
		return this.categoryPolarityScores;

	}

	public void setAspectCategoryPolarities(TravelReview review, Map<String,Double> map){
		review.setAmbianceAspectScore(map.get("AMBIANCE"));
		review.setEntertainmentAspectScore(map.get("ENTERTAINMENT"));
		review.setCostAspectScore(map.get("COST"));
		review.setAccommodationAspectScore(map.get("ACCOMMODATION"));
		review.setSceneryAspectScore(map.get("SCENARY"));
		review.setTransportAspectScore(map.get("TRANSPORT"));
	}

	public  List<String> getCategoriesFromStr(String str){
		String [] temp = str.split(",");
		List<String> categoryList = new ArrayList<String>();
		for(String cat:temp){
			categoryList.add(cat);
		}
		return categoryList;
	}

	public  double getSentimentScore(String word){
		double totalScore = 0.0;
		word = word.replaceAll("([^a-zA-Z\\s])", "");
		totalScore += this.sentiWordNet.extract(word.toLowerCase());
		return totalScore;
	}

	public  double convertToStandardScore(double score){
		Double standardScore = 0.0;
		standardScore  = ((score - -5) / (5 - -5)) * (5 - 1) + 1;
		//(Input - InputLow) / (InputHigh - InputLow)) * (OutputHigh - OutputLow) + OutputLow;
		if (standardScore > 10.0){
			standardScore = 10.0;
		}
		return standardScore;
	}
}
