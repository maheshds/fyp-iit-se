package com.iit.fyp.travelplanner;

import edu.stanford.nlp.ie.NERClassifierCombiner;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreLabel;
import java.io.IOException;
import java.util.List;

public class NERDemo2 {

  public static void main(String[] args) throws IOException {
    String serializedClassifier = "c:\\Users\\Mahesh\\Desktop\\fyp\\final-ner-travel-planner-model.ser.gz";
  //  String serializedClassifier2 = "/local/stanford-ner-2015-01-30/classifiers/english.muc.7class.distsim.crf.ser.gz";

    if (args.length > 0) {
      serializedClassifier = args[0];
    }

    NERClassifierCombiner classifier = new NERClassifierCombiner(false, false, 
            serializedClassifier);

    String fileContents = IOUtils.slurpFile("c:\\Users\\Mahesh\\Desktop\\test-ner.txt").toLowerCase();
    List<List<CoreLabel>> out = classifier.classify(fileContents);

    int i = 0;
    for (List<CoreLabel> lcl : out) {
      i++;
      int j = 0;
      for (CoreLabel cl : lcl) {
        j++;
        System.out.printf("%d:%d: %s%n", i, j,
                cl.toShorterString("Text", "CharacterOffsetBegin", "CharacterOffsetEnd", "NamedEntityTag"));
      }
    }
  }

}