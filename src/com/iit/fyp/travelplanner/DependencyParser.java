package com.iit.fyp.travelplanner;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.BasicDependenciesAnnotation;
import edu.stanford.nlp.trees.CollinsHeadFinder;
import edu.stanford.nlp.trees.HeadFinder;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import edu.stanford.nlp.util.CoreMap;

public class DependencyParser {

	ObjectMapper mapper;
	List<TravelReview> reviewObjects;
	final HeadFinder headFinder;
	HashMap<String, String> nounPhrases;
	HashMap<String, String> verbPhrases;
	ArrayList<String> nounPhrasesList;
	ArrayList<String> verbPhrasesList;
	HashMap<String, String> nouns;
	PrintWriter out;

	public DependencyParser(ObjectMapper mapper, List<TravelReview> reviewObjects){
		this.mapper = mapper;
		this.reviewObjects = reviewObjects;
		this.headFinder = new CollinsHeadFinder();
		
		this.nouns = new HashMap<>();
		this.out = new PrintWriter(System.out);
	}
	
	public void hashMapToArrayList(HashMap<String, String> phrases, ArrayList<String> phraseList){
		for(String phrase: phrases.keySet()){
			phraseList.add(phrase);
			//System.out.println(phraseList);
		}
	}

	public void parseDependencies() throws IOException{

		Properties props = new Properties(); 
		props.put("annotators", "tokenize, ssplit, parse"); 
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		Annotation annotation;

		for(TravelReview review: this.reviewObjects){
			this.nounPhrases = new HashMap<>();
			this.verbPhrases = new HashMap<>();
			this.nounPhrasesList = new ArrayList<String> ();
			this.verbPhrasesList = new ArrayList<String>();
			annotation = new Annotation(review.review);
			pipeline.annotate(annotation);
			List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
			if (sentences != null && sentences.size() > 0) {

				for(CoreMap sentence: sentences){
					Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);
					SemanticGraph basicDeps = sentence.get(BasicDependenciesAnnotation.class);
					Collection<TypedDependency> typedDeps = basicDeps.typedDependencies();

//					for(TypedDependency obj: typedDeps){
//						System.out.println(obj.toString());
//					}
//					out.println();

					// tree.pennPrint(out);
				//	headFinder.determineHead(tree).pennPrint(out);

					// Create a reusable pattern object 
					String patternStr = "NP < JJ < NN |< NNS | NP< DT < NN | VP < VBG | NP < NN | VP < NN < VBG | VP < VBG < NN";
					TregexPattern patternMW = TregexPattern.compile(patternStr); 
					
					// Run the pattern on one particular tree 
					TregexMatcher matcher = patternMW.matcher(tree); 
					// Iterate over all of the subtrees that matched 
					while (matcher.findNextMatchingNode()) { 
						Tree match = matcher.getMatch(); 

						List<Tree> children = match.getLeaves();
						String s = "";
						for(int i=0; i<children.size();i++){
							s+= children.get(i).pennString().substring(1, 3) + " ";
						}
						dfs(match, tree, headFinder,nounPhrases,verbPhrases,nouns);
					}
//					out.println();
//					System.out.println("nounPhrases -----");
//				
//					printMap(nounPhrases);
//					out.println();
//					System.out.println("verbPhrases------------");
//					printMap(verbPhrases);
					//System.out.println("nouns------------");
				//	printMap(nouns);
					
				}
			}
			hashMapToArrayList(nounPhrases, nounPhrasesList);
			hashMapToArrayList(verbPhrases, verbPhrasesList);
			review.setNounPhrasesList(nounPhrasesList);
			review.setVerbPhrasesList(verbPhrasesList);
			System.out.println(review.getNounPhrasesList());
		}
		
		
	}

	
	public void printReviews(){
		for(TravelReview review: this.reviewObjects){
			System.out.println("Review content");
			System.out.println(review.review);
			System.out.println();
			System.out.println("Noun phrases list");
			System.out.println(review.nounPhrasesList);
		}
	}
	
	public void writeTofile(String path){
	
		try {
			System.out.println("Writing to File...");
			this.mapper.writeValue(new File(path), this.reviewObjects);
			System.out.println("Written to file successfully at fyp/nounPhrasedReviews.json");
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void printMap(HashMap<String, String> example){



		for (String name: example.keySet()){

			String key =name.toString();
			String value = example.get(name).toString();  
			System.out.println(" phrase " + key + " " + "head " + value);  


		} 
	}


	public void dfs(Tree node, Tree parent, HeadFinder headFinder,HashMap<String, String> nounPhrases,HashMap<String, String> verbPhrases,HashMap<String, String> nouns) {
		if (node == null || node.isLeaf()) {

			return;
		}
		//if node is a NP - Get the terminal nodes to get the words in the NP      
		if(node.value().equals("NP")) {

			//System.out.println(" Noun Phrase is ");
			List<Tree> leaves = node.getLeaves();

			String phrase ="";
			for(Tree leaf : leaves) {
				phrase += leaf.toString() + " ";
			}

			String head = node.headTerminal(headFinder, parent).toString();
			//System.out.println("noun phrase is " + phrase);
			//System.out.println("head is " + head);
			nounPhrases.put(phrase, head);

		} else if(node.value().equals("VP")) {
			//System.out.println(" Verb Phrase is ");
			List<Tree> leaves = node.getLeaves();

			String phrase ="";
			for(Tree leaf : leaves) {
				phrase += leaf.toString() + " ";
			}
			String head = node.headTerminal(headFinder, parent).toString();
			nounPhrases.put(phrase, head); //verbphrase edit
		}

//		else if(node.value().equals("NN")) {
//			//System.out.println(" Verb Phrase is ");
//			List<Tree> leaves = node.getLeaves();
//
//			String phrase ="";
//			for(Tree leaf : leaves) {
//				phrase += leaf.toString() + " ";
//			}
//			String head = node.headTerminal(headFinder, parent).toString();
//			nouns.put(phrase, head);
//		}
//
//		for(Tree child : node.children()) {
//			dfs(child, node, headFinder,nounPhrases,verbPhrases,nouns);
//		}

	}

}
