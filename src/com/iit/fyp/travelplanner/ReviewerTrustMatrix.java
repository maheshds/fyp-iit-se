package com.iit.fyp.travelplanner;

import java.io.IOException;
import java.util.List;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

public class ReviewerTrustMatrix {
	
	Double avgScenaryAspectScore;
	Double avgAmbianceAspectScore;
	Double avgEntertainmentAspectScore;
	Double avgCostAspectScore;
	Double avgAccommodationAspectScore;
	

	public Double calculateReviewerTrust(List<TravelReview> reviewArr, TravelReview review){
		String fileName = "fuzzy/trustMatrix.fcl";
		FIS fis = FIS.load(fileName, true);
		if (fis == null) { // Error while loading?
			System.err.println("Can't load file: '" + fileName + "'");
	
		}

		// Show ruleset
		FunctionBlock functionBlock = fis.getFunctionBlock(null);
	//	JFuzzyChart.get().chart(functionBlock);

		//calculate averageAspectScores
		calculateAverageAspectScores(reviewArr,review);
		
		// Set inputs
		functionBlock.setVariable("reviewLength", review.review.length());
		functionBlock.setVariable("aspectCoverage", review.getAspectCoverage());
		functionBlock.setVariable("reviewCount", Double.parseDouble(review.getReviewCount()));
		functionBlock.setVariable("scenaryAspectScore", review.getSceneryAspectScore());
		functionBlock.setVariable("ambianceAspectScore", review.getAmbianceAspectScore());
		functionBlock.setVariable("entertainmentAspectScore", review.getEntertainmentAspectScore());
		functionBlock.setVariable("costAspectScore", review.getCostAspectScore());
		functionBlock.setVariable("accommodationAspectScore", review.getAccommodationAspectScore());
		functionBlock.setVariable("avgScenaryAspectScore", this.avgScenaryAspectScore);
		functionBlock.setVariable("avgAmbianceAspectScore", this.avgAmbianceAspectScore);
		functionBlock.setVariable("avgEntertainmentAspectScore", this.avgEntertainmentAspectScore);
		functionBlock.setVariable("avgCostAspectScore", this.avgCostAspectScore);
		functionBlock.setVariable("avgAccommodationAspectScore", this.avgAccommodationAspectScore);

		// Evaluate 
		functionBlock.evaluate();
		
		// Show output variable's chart
		Variable reviewerTrust = functionBlock.getVariable("reviewerTrust");
//		Variable reviewLength = functionBlock.getVariable("reviewLength");
//		Variable aspectCoverage = functionBlock.getVariable("aspectCoverage");
//		Variable reviewCount = functionBlock.getVariable("reviewCount");
//		Variable scenaryAspectScore = functionBlock.getVariable("scenaryAspectScore");
//		Variable ambianceAspectScore = functionBlock.getVariable("ambianceAspectScore");
//		Variable entertainmentAspectScore = functionBlock.getVariable("entertainmentAspectScore");
//		Variable costAspectScore = functionBlock.getVariable("costAspectScore");
//		Variable accommodationAspectScore = functionBlock.getVariable("accommodationAspectScore");
//		JFuzzyChart.get().chart(reviewLength, true);
//		JFuzzyChart.get().chart(aspectCoverage, true);
//		JFuzzyChart.get().chart(reviewCount, true);
//		JFuzzyChart.get().chart(scenaryAspectScore, true);
//		JFuzzyChart.get().chart(ambianceAspectScore, true);
//		JFuzzyChart.get().chart(entertainmentAspectScore, true);
//		JFuzzyChart.get().chart(costAspectScore, true);
//		JFuzzyChart.get().chart(accommodationAspectScore, true);
//		try {
//			System.in.read();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//JFuzzyChart.get().chart(reviewerTrust, reviewerTrust.getDefuzzifier(), true);
		//Gpr.debug("poor[service]: " + functionBlock.getVariable("service").getMembership("poor"));

		// Print ruleSet
		//System.out.println(functionBlock);
		//System.out.println("reviewerTrust:" + functionBlock.getVariable("reviewerTrust").getValue());
		//return functionBlock.getVariable("reviewerTrust").getValue();
		return functionBlock.getVariable("reviewerTrust").getLatestDefuzzifiedValue();
	}
	
	public void calculateAverageAspectScores(List<TravelReview> arr, TravelReview review){
		int reviewCount = 0;
		
		this.avgScenaryAspectScore = 0.0;
		this.avgAmbianceAspectScore = 0.0;
		this.avgEntertainmentAspectScore = 0.0;
		this.avgCostAspectScore = 0.0;
		this.avgAccommodationAspectScore = 0.0;
		
		for(TravelReview reviewObj: arr){
			if(review.getAttractionName().equals(reviewObj.getAttractionName())){
				reviewCount++;
				this.avgScenaryAspectScore += reviewObj.getSceneryAspectScore();
				this.avgAccommodationAspectScore += reviewObj.getAccommodationAspectScore();
				this.avgAmbianceAspectScore += reviewObj.getAmbianceAspectScore();
				this.avgEntertainmentAspectScore += reviewObj.getEntertainmentAspectScore();
				this.avgCostAspectScore += reviewObj.getCostAspectScore();
				//System.out.println(this.avgScenaryAspectScore + " " + this.avgAmbianceAspectScore + " " + this.avgEntertainmentAspectScore + " " + this.avgCostAspectScore + " " + this.avgAccommodationAspectScore);
			}
			
		}
		//System.out.println(reviewCount);
		this.avgScenaryAspectScore = this.avgScenaryAspectScore / reviewCount;
		this.avgAmbianceAspectScore = this.avgAmbianceAspectScore / reviewCount;
		this.avgEntertainmentAspectScore = this.avgEntertainmentAspectScore / reviewCount;
		this.avgCostAspectScore = this.avgCostAspectScore / reviewCount;
		this.avgAccommodationAspectScore = this.avgAccommodationAspectScore / reviewCount;
		//System.out.println(this.avgScenaryAspectScore + " " + this.avgAmbianceAspectScore + " " + this.avgEntertainmentAspectScore + " " + this.avgCostAspectScore + " " + this.avgAccommodationAspectScore);
		
	}
}
