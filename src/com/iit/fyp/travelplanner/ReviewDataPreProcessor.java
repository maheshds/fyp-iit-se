package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opencsv.CSVReader;

public class ReviewDataPreProcessor {

	CSVReader reader;
	ObjectMapper mapper;
	
	public ReviewDataPreProcessor(CSVReader reader,ObjectMapper mapper){
		this.reader = reader;
		this.mapper = mapper;
	}
	
	public List<TravelReview> generateUUIDForReviews(){
		try {
			//int count = 0;
			String[] nextLine;
			this.reader.readNext();
			ArrayList<TravelReview> arrayList = new ArrayList<TravelReview>();
			while ((nextLine = this.reader.readNext()) != null) {
			TravelReview review = new TravelReview(nextLine);
			//add UUID to review
			review.setReviewId(UUID.randomUUID());
			arrayList.add(review);
			
			}

			return arrayList;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public  void convertCsvtoJson (ArrayList<TravelReview> list,ObjectMapper mapper){
		
		try {
			mapper.writeValue(new File("c:\\Users\\Mahesh\\Desktop\\fyp\\updated-json-dataset.json"), list);
			System.out.println("writing to fyp/updated-json-dataset.json - Success!");
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
