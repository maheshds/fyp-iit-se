package com.iit.fyp.travelplanner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.io.ObjectInputStream;
import java.io.IOException;

import edu.stanford.nlp.classify.Classifier;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.GeneralDataset;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.objectbank.ObjectBank;
import edu.stanford.nlp.util.ErasureUtils;
import edu.stanford.nlp.util.Pair;

class ClassifierDemo {

  public static void main(String[] args) throws Exception {

 
	  String [] strarr = {"Peaceful and religious place Best place to know the history  religion and about the people of Kandy Huge place take some time to go around the place. \t"};


	  	MaxEntClassifier classifierObj = new MaxEntClassifier();
	 // 	classifierObj.trainClassifier("examples/travel.prop", "examples/travel.train", "examples/travel.model");
	  	@SuppressWarnings("unchecked")
	  	ColumnDataClassifier cdc = new ColumnDataClassifier("examples/travel.prop");
	  	GeneralDataset<String, String> dataset = cdc.readTrainingExamples("examples/travel.train");
	  	GeneralDataset<String, String> testSet = cdc.readTrainingExamples("examples/travel.test");
	  	Classifier<String, String> lc  = cdc.makeClassifier(dataset);
	  	//GeneralDataset<String, String> testData = cdc.readTrainingExamples("examples/travel.train");	
//	  	Collection<String> collObj = lc.labels();
//	  	Iterator<String> iterator = collObj.iterator();
//	  	while(iterator.hasNext()){
//	  		//System.out.println(iterator.next());
//	  	}
	  	System.out.println(lc.evaluateAccuracy(testSet));
	  	System.out.println(lc.evaluatePrecisionAndRecall(testSet, "SCENARY"));
      for (String li : strarr){
          Datum<String, String> d = cdc.makeDatumFromLine(li);
          
          System.out.println(li + "  ==>  " + lc.classOf(d)+ " (score: " + lc.scoresOf(d) + ")");
          //+ " (score: " + cl.scoresOf(d) + ")");
      }

  }


//  public static void demonstrateSerialization()
//    throws IOException, ClassNotFoundException {
//    System.out.println("Demonstrating working with a serialized classifier");
//    ColumnDataClassifier cdc = new ColumnDataClassifier("examples/cheese2007.prop");
//    Classifier<String,String> cl =
//        cdc.makeClassifier(cdc.readTrainingExamples("examples/cheeseDisease.train"));
//
//    // Exhibit serialization and deserialization working. Serialized to bytes in memory for simplicity
//    System.out.println(); System.out.println();
//    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//    ObjectOutputStream oos = new ObjectOutputStream(baos);
//    oos.writeObject(cl);
//    oos.close();
//    File yourFile = new File("examples/cheeseDisease.model");
//    if(!yourFile.exists()) {
//        yourFile.createNewFile();
//    } 
//    FileOutputStream oFile = new FileOutputStream(yourFile, false); 
//    ObjectOutputStream oos2 = new ObjectOutputStream(oFile);
//    oos2.writeObject(cl);
//    oos2.close();
//    
//    byte[] object = baos.toByteArray();
//    ByteArrayInputStream bais = new ByteArrayInputStream(object);
//    ObjectInputStream ois = new ObjectInputStream(bais);
//    LinearClassifier<String,String> lc = ErasureUtils.uncheckedCast(ois.readObject());
//    ois.close();
//    ColumnDataClassifier cdc2 = new ColumnDataClassifier("examples/cheese2007.prop");
//
//    // We compare the output of the deserialized classifier lc versus the original one cl
//    // For both we use a ColumnDataClassifier to convert text lines to examples
//    for (String line : ObjectBank.getLineIterator("examples/cheeseDisease.test", "utf-8")) {
//      Datum<String,String> d = cdc.makeDatumFromLine(line);
//      Datum<String,String> d2 = cdc2.makeDatumFromLine(line);
//      System.out.println(line + "  =origi=>  " + cl.classOf(d));
//      System.out.println(line + "  =deser=>  " + lc.classOf(d2));
//    }
//  }

  
}