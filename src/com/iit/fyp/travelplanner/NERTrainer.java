package com.iit.fyp.travelplanner;

import java.util.Properties;

import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.sequences.SeqClassifierFlags;
import edu.stanford.nlp.util.StringUtils;

public class NERTrainer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String prop = "c:\\Users\\Mahesh\\Downloads\\properties.prop";
		Properties props = StringUtils.propFileToProperties(prop);
		String to = props.getProperty("serializeTo");
		props.setProperty("trainFile", "c:\\Users\\Mahesh\\Desktop\\fyp\\NER-train-file.txt");
		props.setProperty("testFile", "c:\\Users\\Mahesh\\Desktop\\fyp\\NER-test-file.txt");
		props.setProperty("serializeTo", "c:\\Users\\Mahesh\\Desktop\\fyp\\final-ner-travel-planner-model.ser.gz");
		SeqClassifierFlags flags = new SeqClassifierFlags(props);
		CRFClassifier<CoreLabel> crf = new CRFClassifier<CoreLabel>(flags);
	
		crf.train();
		crf.serializeClassifier("c:\\Users\\Mahesh\\Desktop\\fyp\\final-ner-travel-planner-model.ser.gz");
		
	}

}
