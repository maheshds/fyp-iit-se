package com.iit.fyp.travelplanner;

import java.util.List;

public class WeightAndSentimentAllocator {

	Double scenaryAspectWeight;
	Double accommodationAspectWeight;
	Double ambianceAspectWeight;
	Double entertainmentAspectWeight;
	Double costAspectWeight;

	public void allocateWeightsAndSentiments(List<TravelReview> reviewObjects ,TravelReview review){
		////////FUZZY LOGIC - REVIEWER TRUST ////////
		generateTrustMatrix(reviewObjects, review);

		////////////////FUZZY LOGIC - ASPECT WEIGHT AND FINAL SCORE ALLOCATION //////////////////
		calculateWeightsAndSentiments(review);
	}

	public void generateTrustMatrix (List<TravelReview> reviewObjects, TravelReview review){
		System.out.println(review.review);
		System.out.println(review.getAspectTermsList());
		ReviewerTrustMatrix reviewerObj = new ReviewerTrustMatrix();
		Double reviewerTrust = reviewerObj.calculateReviewerTrust(reviewObjects, review);
		review.setReviewerTrust(reviewerTrust);
		System.out.println("Reviewer trust" + review.getReviewerTrust());
	}

	public void calculateWeightsAndSentiments(TravelReview review){
		scenaryAspectWeight = 0.0;
		accommodationAspectWeight = 0.0;
		ambianceAspectWeight = 0.0;
		entertainmentAspectWeight =0.0;
		costAspectWeight = 0.0;

		AspectWeightAllocator weightObj = new AspectWeightAllocator();
		FinalSentimentCalculator sentimentObj = new FinalSentimentCalculator();

		for(String category:review.getAspectCategoriesList()){
			switch(category){
			case "SCENARY":
				scenaryAspectWeight = weightObj.calculateAspectWeight(review, review.getSceneryAspectScore());
				Double finalScenaryCategoryScore = sentimentObj.calculateFinalSentiment(review, review.getSceneryAspectScore(), scenaryAspectWeight);
				review.setFinalScenaryAspectScore(finalScenaryCategoryScore);

				break;
			case "ACCOMMODATION":
				accommodationAspectWeight = weightObj.calculateAspectWeight(review, review.getAccommodationAspectScore());
				Double finalAccommodationCategoryScore = sentimentObj.calculateFinalSentiment(review, review.getAccommodationAspectScore(), accommodationAspectWeight);
				review.setFinalAccommodationAspectScore(finalAccommodationCategoryScore);
				break;
			case "AMBIANCE":
				ambianceAspectWeight = weightObj.calculateAspectWeight(review, review.getAmbianceAspectScore());
				Double finalAmbianceCategoryScore = sentimentObj.calculateFinalSentiment(review, review.getAmbianceAspectScore(), ambianceAspectWeight);
				review.setFinalAmbianceAspectScore(finalAmbianceCategoryScore);
				break;
			case "ENTERTAINMENT":
				entertainmentAspectWeight = weightObj.calculateAspectWeight(review, review.getEntertainmentAspectScore());
				Double finalEntertainmentCategoryScore = sentimentObj.calculateFinalSentiment(review, review.getEntertainmentAspectScore(), entertainmentAspectWeight);
				review.setFinalEntertainmentAspectScore(finalEntertainmentCategoryScore);
				break;
			case "COST":
				costAspectWeight = weightObj.calculateAspectWeight(review, review.getCostAspectScore());
				Double finalCostCategoryScore = sentimentObj.calculateFinalSentiment(review, review.getCostAspectScore(), costAspectWeight);
				review.setFinalCostAspectScore(finalCostCategoryScore);
				break;

			}
		}

	}
}
