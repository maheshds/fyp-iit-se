package com.iit.fyp.travelplanner;

public class SentiWordNet {

	String pos;
	String word;
	String positiveScore;
	String negativeScore;
	String objectiveScore;
	
	public SentiWordNet(String [] row){
		this.pos = row[0];
		this.word = row[1];
		this.positiveScore = row[3];
		this.negativeScore = row[4];
		this.objectiveScore = row[5];
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getPositiveScore() {
		return positiveScore;
	}

	public void setPositiveScore(String positiveScore) {
		this.positiveScore = positiveScore;
	}

	public String getNegativeScore() {
		return negativeScore;
	}

	public void setNegativeScore(String negativeScore) {
		this.negativeScore = negativeScore;
	}

	public String getObjectiveScore() {
		return objectiveScore;
	}

	public void setObjectiveScore(String objectiveScore) {
		this.objectiveScore = objectiveScore;
	}
	
	
}
