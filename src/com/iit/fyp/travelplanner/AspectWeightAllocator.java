package com.iit.fyp.travelplanner;

import java.io.IOException;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

public class AspectWeightAllocator {

	
	public Double calculateAspectWeight(TravelReview review, Double aspectCategoryScore){
		String fileName = "fuzzy/weightAllocator.fcl";
		FIS fis = FIS.load(fileName, true);
		if (fis == null) { // Error while loading?
			System.err.println("Can't load file: '" + fileName + "'");
	
		}

		// Show ruleset
		FunctionBlock functionBlock = fis.getFunctionBlock(null);
	//	JFuzzyChart.get().chart(functionBlock);

		;
		
		// Set inputs
		functionBlock.setVariable("aspectCategoryScore", aspectCategoryScore);
		functionBlock.setVariable("reviewerTrust", review.getReviewerTrust());
	

		// Evaluate 
		functionBlock.evaluate();

		// Show output variable's chart
//		Variable aspectCategoryScore2 = functionBlock.getVariable("aspectCategoryScore");
//		Variable reviewerTrust = functionBlock.getVariable("reviewerTrust");
//		JFuzzyChart.get().chart(aspectCategoryScore2, true);
//		JFuzzyChart.get().chart(reviewerTrust, true);
//		try {
//			System.in.read();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//Gpr.debug("poor[service]: " + functionBlock.getVariable("service").getMembership("poor"));

	
		return functionBlock.getVariable("aspectWeight").getLatestDefuzzifiedValue();
	}
}
