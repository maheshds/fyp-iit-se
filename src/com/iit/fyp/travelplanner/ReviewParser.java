package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class ReviewParser {

	ObjectMapper mapper;
	TravelReview[] reviewObjects;
	ArrayList<TravelReview> splitReviewList;
	ArrayList<TravelReview> lemmatizedReviewList;

	public ReviewParser(ObjectMapper mapper,TravelReview[] reviewObjects){
		this.mapper = mapper;
		this.reviewObjects = reviewObjects;
		this.splitReviewList = null;
		this.lemmatizedReviewList = null;
	}

	public void spiltSentences(){
		//threviewObjects = this.mapper.readValue(new File("c:\\Users\\Mahesh\\Desktop\\test.json"), TravelReview[].class);
		//System.out.println(reviewObjects[0].review);
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize ssplit");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		Annotation annotation;
		this.splitReviewList = new ArrayList<TravelReview>();

		for(TravelReview review: this.reviewObjects){
			annotation = new Annotation(review.review);
			pipeline.annotate(annotation);
			List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
			ArrayList<String> segmentsList = new ArrayList<String>();

			for(int i=0; i<sentences.size();i++){
				segmentsList.add(sentences.get(i).toString());
			}

			//review.setSegmentsArr(segmentsList);
			this.splitReviewList.add(review);

		}
		//this.mapper.writeValue(new File("c:\\Users\\Mahesh\\Desktop\\test13.json"), modifiedReviewList);
		System.out.println("Splitting -- Success!");
	}

	
	public void lemmaAndPosTagger(){
		
		Properties props = new Properties(); 
		props.put("annotators", "tokenize, ssplit, pos, lemma"); 
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		Annotation annotation;
		this.lemmatizedReviewList = new ArrayList<TravelReview>();

		for(TravelReview review: this.reviewObjects){
			annotation = pipeline.process(review.review);  	
			List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
			ArrayList<String> posTaggedList = new ArrayList<String>();

			for(CoreMap sentence: sentences)
		    {    
		    	String sentenceStr ="";
		        for(CoreLabel token: sentence.get(TokensAnnotation.class))
		        {       
		            
		          //  String lemma = token.get(LemmaAnnotation.class); 
		            String pos = token.get(PartOfSpeechAnnotation.class);
		            sentenceStr += token.toString()  + "(" + pos + ") ";
		            
		            
		        }
		     posTaggedList.add(sentenceStr);
		    }
			
			//review.setPosTaggedArr(posTaggedList);
			this.lemmatizedReviewList.add(review);
		}
//	this.mapper.writeValue(new File("c:\\Users\\Mahesh\\Desktop\\test14.json"), modifiedReviewList);
		System.out.println("Lemmatization & POS tagging -- Success!");
	}
	
	public void writeTofile(){
		for(int i=0; i< this.reviewObjects.length;i++){
			TravelReview review = this.reviewObjects[i];
			//review.setSegmentsArr(this.splitReviewList.get(i).getSegmentsArr());
			//review.setPosTaggedArr(this.lemmatizedReviewList.get(i).getPosTaggedArr());
		}
		
		try {
			System.out.println("Writing to File...");
			this.mapper.writeValue(new File("c:\\Users\\Mahesh\\Desktop\\split&POSTagged.json"), this.reviewObjects);
			System.out.println("Written to file successfully.");
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}


