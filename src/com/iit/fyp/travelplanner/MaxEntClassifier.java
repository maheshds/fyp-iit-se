package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import edu.stanford.nlp.classify.Classifier;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.LinearClassifier;

public class MaxEntClassifier {
	
	Classifier<String, String> classifier;
	ColumnDataClassifier columnDataClassifier; 

	public Classifier<String, String> getClassifier() {
		return classifier;
	}

	public void setClassifier(Classifier<String, String> classifier) {
		this.classifier = classifier;
	}

	public ColumnDataClassifier getColumnDataClassifier() {
		return columnDataClassifier;
	}

	public void setColumnDataClassifier(ColumnDataClassifier columnDataClassifier) {
		this.columnDataClassifier = columnDataClassifier;
	}

	public void trainClassifier(String propertyFile, String trainFile, String filePath){
		ColumnDataClassifier cdc = new ColumnDataClassifier(propertyFile);
		System.out.println("Property file :" + propertyFile + " loaded..");
		Classifier<String, String> cl
		= cdc.makeClassifier(cdc.readTrainingExamples(trainFile));
		
		System.out.println("Classifier training successful.");
		File classifierPath = new File(filePath);
		if(!classifierPath.exists()) {
			try {

				classifierPath.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		FileOutputStream oFile;
		try {
			System.out.println("Writing to file..");
			oFile = new FileOutputStream(classifierPath, false);
			ObjectOutputStream oos2 = new ObjectOutputStream(oFile);
			oos2.writeObject(cl);
			oos2.close();
			System.out.println("Classifier written to file successfully at: " + classifierPath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public LinearClassifier<String, String> loadClassifier(String filePath) throws FileNotFoundException, IOException, ClassNotFoundException{
		File classifierPath = new File(filePath);
		LinearClassifier<String, String> classifier;
		ObjectInputStream ois =
				new ObjectInputStream (new FileInputStream (classifierPath));
		classifier = (LinearClassifier<String, String>) ois.readObject();
		ois.close();
		
		return classifier;
	}



}
