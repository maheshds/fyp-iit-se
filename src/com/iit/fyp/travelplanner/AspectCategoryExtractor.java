package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale.Category;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class AspectCategoryExtractor {

	private static final String [] ACCOMMODATION = {"hotel","restaurant","cafe","tea shop","lodging","coffee","tea","beer",
			"drink","alcohol","eateries","food","hosts","rented ","house","rented apartment","apartment","hostel","pub",
			"bar","snack"};

	private static final String [] TRANSPORT = {"car","van","ride","walk","traffic","travel","road","bike",
			"public transport","bus","highway","expressway","tuk tuk","tuktuk","tucktuck",
			"tuk","roads","took took","path","foot path","public","transport","train","locomotive",
			"taxi","uber","metered","cabbie","fare","hail","climb","trail","stroll","alley"};

	private static final String [] AMBIANCE = {"ambiance","ambience","cool","warm","hot","atmosphere","surroundings","vibe","decor","feel",
			"style","bistro","pub","style","environment","casual","cozy","elegant",
			"relax","relaxed","romantic","overall","mood","smell","spiritual","spirit","setting","climate","dull",
			"energetic","jungle","hood","habitat","noisy","quiet","calm","serene","chill","chilling","relaxing","calming",
			"chillax","lighten","sedate","soothe","soothing","architecture","crowded","clean","dirty", "humid" , "sunny", "overcast","cloudy"};

	private static final String [] COST = {"costly","expensive","price","inexpensive","fee","ticket","expenditure","rental","rent","saving",
			"lowcost","sales","sale","charge","payment","free","overpriced",
			"over-priced","bargain","buy","buying"};

	private static final String [] CULTURAL_SENSITIVITY = {"museum","colonial","historical","history","pagoda","temple","monk","worship",
			"heritage","statue","tradition","archaic","cultured","culture","folklore","community","national","church","mosque","Ceylon",
			"palace","Kandyan","kingdom","buddhist","hindu","islam","christian","catholic","king","queen","stupa","festival","European",
			"Dutch","Portugese","perahera","paintings","sculptures"};

	private static final String [] ENTERTAINMENT = {"snorkelling","snorkel", "swim", "swimming", "surf", "surfing" ,"music","dance","dancing","party","dj","singing",
			"gypsy", "birds", "snake charmer", "bath", "photography","game","cricket","soccer","football","boat","scuba", "diving","trek", "picnik","explore"};

	private static final String [] SCENARY = {"backdrop","scene","sight","scenery","sunset","views","view","Views","landscape","lake","panorama","panoramic","scenic","skyline",
			"viewpoint","ramparts","fortifications","sun rise","sun set","captivating","mesmerizing",
			"waterfall","ocean","beach","waves","decor","nature","jungle","sea","romantic","tank", "river"};



	Properties props;
	StanfordCoreNLP pipeline;
	Annotation annotation;
	Map<Set<String>, String> map;
	Set<String> aspectCategories;
	Datum<String, String> datum;
	List<String> aspectCategoryList;


	public AspectCategoryExtractor(){
		//this.mapper = new ObjectMapper();
		//this.reviewObjects = mapper.readValue(new File("c:\\Users\\Mahesh\\Desktop\\fyp\\final-train-data.json"), TravelReview[].class);
		//this.reviewObjects = reviewObjects;
		Set<String> accommodationWords = new HashSet<>(Arrays.asList(ACCOMMODATION));
		Set<String> transportWords = new HashSet<>(Arrays.asList(TRANSPORT));
		Set<String> ambianceWords = new HashSet<>(Arrays.asList(AMBIANCE));
		Set<String> costWords = new HashSet<>(Arrays.asList(COST));
		Set<String> cultureWords = new HashSet<>(Arrays.asList(CULTURAL_SENSITIVITY));
		Set<String> entertainmentWords = new HashSet<>(Arrays.asList(ENTERTAINMENT));
		Set<String> scenaryWords = new HashSet<>(Arrays.asList(SCENARY));
		this.map = new HashMap<>();
		this.map.put(accommodationWords, "ACCOMMODATION");
		this.map.put(transportWords, "TRANSPORT");
		this.map.put(ambianceWords, "AMBIANCE");
		this.map.put(costWords, "COST");
		this.map.put(cultureWords, "CULTURE");
		this.map.put(entertainmentWords, "ENTERTAINMENT");
		this.map.put(scenaryWords, "SCENARY");

		this.props = new Properties();
		this.props.setProperty("annotators", "tokenize, ssplit,pos,lemma");
		this.pipeline = new StanfordCoreNLP(props);
		this.annotation = null;
		this.aspectCategories = null;
		this.datum = null;
		this.aspectCategoryList = null;
	}

	public List<String>  categorizeReviewAspects(TravelReview review){

		String prepReviewStr = review.review.replaceAll("\\.", "\\. ");
		this.annotation = new Annotation(prepReviewStr);
		this.pipeline.annotate(this.annotation);
		List<String> reviewAspectOccurenceList = new ArrayList<String>();
		List<CoreMap> sentences = this.annotation.get(SentencesAnnotation.class);

		for(CoreMap sentence:sentences){
			String lemmatizedSentence = null;
			for(CoreLabel token: sentence.get(TokensAnnotation.class)){
				lemmatizedSentence +=  token.lemma() + " ";

			}

			@SuppressWarnings("unchecked")
			Set<Category> categories = (Set<Category>) Arrays.stream(lemmatizedSentence.toLowerCase().split("\\s"))
			.map(s -> {
				for (Set<String> keywords : this.map.keySet()) {
					if (keywords.contains(s)) {

						//System.out.println(s);
						//System.out.println(map.get(keywords));
						return Optional.of(this.map.get(keywords));
					}
				}
				return Optional.<Category>empty();
			})
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toSet());

			Object[] arr = categories.toArray();
			String categoryStr = "";
			for(Object obj:arr){
				categoryStr += obj + " ";
			}
			//System.out.println(categoryStr);
			reviewAspectOccurenceList.add(categoryStr);
			//String taggedSentenceStr = sentence.toString()  + "," + categoryStr +  "\n";


		}
		return reviewAspectOccurenceList;
	}

	public int getAspectCoverage(List<String> reviewAspectOccurenceList){

		Set<String> aspectCategories = new HashSet<>();
		for(String categoryObj: reviewAspectOccurenceList){
			String [] splitArr = categoryObj.split(" ");
			for(String category:splitArr){
				if(!category.equals("")){
					aspectCategories.add(category);
				}
			}
		}
		return aspectCategories.size();
	}

	public void writeToFile(String taggedSentenceStr){
		try
		{

			FileWriter fw = new FileWriter("c:\\Users\\Mahesh\\Desktop\\fyp\\weka.txt",true); //the true will append the new data
			fw.write(taggedSentenceStr);//appends the string to the file
			fw.close();
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}
	}

	public List<String> extractAspectCategoriesFromReview(TravelReview review, LinearClassifier<String,String> lc, ColumnDataClassifier cdc) throws FileNotFoundException, ClassNotFoundException, IOException{

		this.annotation = new Annotation(review.review);
		this.pipeline.annotate(this.annotation);
		List<CoreMap> sentences = this.annotation.get(CoreAnnotations.SentencesAnnotation.class);
		this.aspectCategories = new HashSet<>();

		for(CoreMap sentence:sentences){
			this.datum = cdc.makeDatumFromLine(sentence.toString() + "\t");
			String categoryStr = lc.classOf(this.datum);
			String [] categoryArr = categoryStr.split(",");
			for(String category: categoryArr){
				this.aspectCategories.add(category.trim());
			}

		}


		@SuppressWarnings("rawtypes")
		Iterator iterator = this.aspectCategories.iterator(); 
		this.aspectCategoryList = new ArrayList<String>();
		while (iterator.hasNext()){
			this.aspectCategoryList.add(iterator.next().toString());
		}

		return this.aspectCategoryList;
	}


}
