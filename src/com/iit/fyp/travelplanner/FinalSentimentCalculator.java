package com.iit.fyp.travelplanner;

import java.io.IOException;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

public class FinalSentimentCalculator {

	
	public Double calculateFinalSentiment(TravelReview review, Double aspectCategoryScore, Double aspectWeight){
		String fileName = "fuzzy/finalScoreAllocator.fcl";
		FIS fis = FIS.load(fileName, true);
		if (fis == null) { // Error while loading?
			System.err.println("Can't load file: '" + fileName + "'");
	
		}

		// Show ruleset
		FunctionBlock functionBlock = fis.getFunctionBlock(null);
	//	JFuzzyChart.get().chart(functionBlock);

		;
		// Set inputs
		functionBlock.setVariable("aspectCategoryScore", aspectCategoryScore);
		functionBlock.setVariable("aspectWeight", aspectWeight);
	

		// Evaluate 
		functionBlock.evaluate();

		// Show output variable's chart
		Variable aspectCategoryScore2 = functionBlock.getVariable("aspectCategoryScore");
		Variable aspectWeight2 = functionBlock.getVariable("aspectWeight");
		JFuzzyChart.get().chart(aspectCategoryScore2, true);
		JFuzzyChart.get().chart(aspectWeight2, true);
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Gpr.debug("poor[service]: " + functionBlock.getVariable("service").getMembership("poor"));

		// Print ruleSet
		//System.out.println(functionBlock);
		//System.out.println("aspectCategoryWeight:" + functionBlock.getVariable("aspectWeight").getValue());
		//return functionBlock.getVariable("finalAspectCategoryScore").getValue();

		 return functionBlock.getVariable("finalAspectCategoryScore").getLatestDefuzzifiedValue();
	
	}
}
