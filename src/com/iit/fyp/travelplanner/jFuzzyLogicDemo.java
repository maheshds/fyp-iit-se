package com.iit.fyp.travelplanner;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.Gpr;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

/**
 * Test parsing an FCL file
 * @author pcingola@users.sourceforge.net
 */
public class jFuzzyLogicDemo {

	public static void main(String[] args) throws Exception {
		// Load from 'FCL' file
		String fileName = "fuzzy/trustMatrix.fcl";
		FIS fis = FIS.load(fileName, true);
		if (fis == null) { // Error while loading?
			System.err.println("Can't load file: '" + fileName + "'");
			return;
		}

		// Show ruleset
		FunctionBlock functionBlock = fis.getFunctionBlock(null);
	//	JFuzzyChart.get().chart(functionBlock);

		// Set inputs
		functionBlock.setVariable("reviewLength", 233);
		functionBlock.setVariable("aspectCoverage", 1);
		functionBlock.setVariable("reviewCount", 45);
		functionBlock.setVariable("scenaryAspectScore", 4);
		functionBlock.setVariable("ambianceAspectScore", 3);
		functionBlock.setVariable("entertainmentAspectScore", 5);
		functionBlock.setVariable("costAspectScore", 3);
		functionBlock.setVariable("accommodationAspectScore", 4);
		functionBlock.setVariable("avgScenaryAspectScore", 4);
		functionBlock.setVariable("avgAmbianceAspectScore", 3.4);
		functionBlock.setVariable("avgEntertainmentAspectScore", 4.1);
		functionBlock.setVariable("avgCostAspectScore", 4.3);
		functionBlock.setVariable("avgAccommodationAspectScore", 4);

		// Evaluate 
		functionBlock.evaluate();

		// Show output variable's chart
		Variable reviewerTrust = functionBlock.getVariable("reviewerTrust");
		//JFuzzyChart.get().chart(reviewerTrust, reviewerTrust.getDefuzzifier(), true);
		//Gpr.debug("poor[service]: " + functionBlock.getVariable("service").getMembership("poor"));

		// Print ruleSet
		//System.out.println(functionBlock);
		System.out.println("reviewerTrust:" + functionBlock.getVariable("reviewerTrust").getValue());
	}
}
