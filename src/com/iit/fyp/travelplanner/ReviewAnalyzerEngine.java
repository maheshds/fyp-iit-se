package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.codehaus.jackson.map.ObjectMapper;

import com.opencsv.CSVReader;

import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.classify.LinearClassifier;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class ReviewAnalyzerEngine {

	public List<TravelReview> analyzeReviews(CSVReader reader, ObjectMapper mapper, List<TravelReview> reviewObjects) throws IOException, ClassNotFoundException{
	
		//Generate UUID for every review.
		
		ReviewDataPreProcessor preProcessorObj = new ReviewDataPreProcessor(reader, mapper);
		reviewObjects = preProcessorObj.generateUUIDForReviews();

		//get noun phrases and verb phrases.
		DependencyParser depParserObj = new DependencyParser(mapper, reviewObjects);
		depParserObj.parseDependencies();

		//Extract Aspect Terms from reviews.

		AspectTermExtractor aspectTermExtractObj = new AspectTermExtractor(reviewObjects,mapper);
		aspectTermExtractObj.getKeywordsFromNounPhrases();
		
		////////ASPECT TERM POLARITY EXTRACTION //////////////////

		extractAspectTermPolarities(reviewObjects);

		/////////////////ASPECT CATEGORY EXTRACTION////////////////////////////////
		
		extractAspectCategories(reviewObjects);
		
		////////////////////////ASPECT CATEGORY POLARITY//////////////////////////////

		extractAspectCategoryPolarities(reviewObjects);
		
		return reviewObjects;
		
	}
	
	public void extractAspectTermPolarities(List<TravelReview> reviewObjects){
		AspectTermPolarityExtractor aspectTermPolarityObj = new AspectTermPolarityExtractor();
		Properties props1 = new Properties();
		props1.setProperty("annotators", "tokenize, ssplit, pos, lemma");
		StanfordCoreNLP pipeline1 = new StanfordCoreNLP(props1);
		Annotation annotation1 = null;
		for(TravelReview review: reviewObjects){


			System.out.println(review.review);
			aspectTermPolarityObj.aspectTermAndAdjectivesMap = new HashMap<String, String>();
			aspectTermPolarityObj.aspectTermsAndSentimentScoresMap = new HashMap<String,Double>();
			Map<String, String> map = aspectTermPolarityObj.extractAdjectives(review,pipeline1,annotation1);

			review.setAspectTermAndAdjectivesMap(map);
			//System.out.println(Collections.singletonList(map));
			aspectTermPolarityObj.processAspectTermsAndAdjectives(review.aspectTermsList,aspectTermPolarityObj.aspectTermAndAdjectivesMap);
			review.setAspectTermsAndSentimentScoresMap(aspectTermPolarityObj.aspectTermsAndSentimentScoresMap);
			System.out.println(Collections.singletonList(review.getAspectTermsAndSentimentScoresMap()));
		}
	}
	
	public void extractAspectCategories(List<TravelReview> reviewObjects) throws FileNotFoundException, ClassNotFoundException, IOException{
		String propFilePath = "examples/travel.prop";
		String classifierPath = "examples/travel.model";
		AspectCategoryExtractor obj = new AspectCategoryExtractor();
		MaxEntClassifier maxEntClassifier = new MaxEntClassifier();
		LinearClassifier<String,String> lc = maxEntClassifier.loadClassifier(classifierPath);
		ColumnDataClassifier cdc = new ColumnDataClassifier(propFilePath);


		for(TravelReview review:reviewObjects){
			List<String> aspectCategoriesList = obj.extractAspectCategoriesFromReview(review, lc, cdc);
			review.setAspectCategoriesList(aspectCategoriesList);
		}

	}
	
	public void extractAspectCategoryPolarities(List<TravelReview> reviewObjects) throws FileNotFoundException, ClassNotFoundException, IOException{
		String propFilePath = "examples/travel.prop";
		String classifierPath = "examples/travel.model";

		MaxEntClassifier maxEntClassifier = new MaxEntClassifier();
		LinearClassifier<String,String> lc = maxEntClassifier.loadClassifier(classifierPath);
		ColumnDataClassifier cdc = new ColumnDataClassifier(propFilePath);
		Properties props =  new Properties();
		props.setProperty("annotators", "tokenize, ssplit,pos,lemma");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		Annotation annotation = null;
		Map<String, Double> aspectCategoryMap = new HashMap<String,Double>();
		AspectCategoryPolarityExtractor aspectCatPolObj = new AspectCategoryPolarityExtractor();

		for(TravelReview review:reviewObjects){

			for(String category:aspectCatPolObj.CATEGORIES){
				aspectCatPolObj.categoryPolarityScores.put(category, 0.0);
			}

			aspectCategoryMap = aspectCatPolObj.extractAspectCategoriesFromReview(review, lc, cdc, annotation, pipeline);
			aspectCatPolObj.setAspectCategoryPolarities(review, aspectCategoryMap);
			//System.out.println(review.getAccommodationAspectScore());

		}
	}
}
