package com.iit.fyp.travelplanner;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;

import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class AspectTermExtractor {

	List<TravelReview> reviewObjects ;
	ObjectMapper mapper;

	ArrayList<String> extractedKeywordsList;
	ArrayList<String> refinedKeywordsList;
	ArrayList<String> nerValidatedKeywordsList;
	List<String> finalizedAspectTermsList;
	Map<String, Double> alchemyExtractedKeywordsList;

	String [] stopWords = {"a","about","above","after","again","against","all","am","an","and","any","are","aren't","as","at","be","because","been","before","being","below","between"
			,"both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't","has"
			,"hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself","his","how","how's","i","i'd","i'll","i'm"
			,"i've","if","in","into","is","isn't","it","it's","its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only"
			,"or","other","ought","our","ours","ourselves","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that","that's"
			,"the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","through","to","too","under","until"
			,"up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while","who","who's","whom","why"
			,"why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves"};



	//Alchemy API keyword JSON deserialization classes.
	public class Keyword {
		public String text;
		public String relevance;
	}


	public class ResponseJSON {
		public String status;
		public String usage;
		public String language;
		public Collection<Keyword> keywords;
	}

	//String [] stopWords = {"a","about","above", "the","theirs","them","themselves","then","there","there's","these","they","they'd"};
	public AspectTermExtractor(List<TravelReview> reviewObjects, ObjectMapper mapper){
		this.reviewObjects = reviewObjects;
		this.mapper = mapper;

	}

	public List<String> removeDuplicateKeywords(ArrayList<String> list){
		//System.out.println(list);
		Set<String> noDuplicates = list
				.stream()
				.filter(
						(s) -> list.stream()
						.noneMatch(
								(ss) -> ss.length() > s.length() && (ss.startsWith(s) || ss.contains(s) )
								)
						)
				.collect(Collectors.toSet());
		//System.out.println(noDuplicates);
		this.refinedKeywordsList = new ArrayList<String>(noDuplicates);
		return this.refinedKeywordsList;

	}

	public void getKeywordsFromNounPhrases() throws IOException{


		Properties props = new Properties(); 
		props.put("annotators", "tokenize, ssplit, pos,lemma, ner");
		props.put("ner.model", "c:\\Users\\Mahesh\\Desktop\\ner-travel-planner-model.ser.gz");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		Annotation annotation;



		for(TravelReview review: reviewObjects){

			this.extractedKeywordsList = new ArrayList<String>();
			ArrayList<String> nounPhraseList = review.getNounPhrasesList();
			for(String nounPhrase: nounPhraseList){

				nounPhrase = removeStopWords(nounPhrase);
				nounPhrase = nounPhrase.replaceAll("\\.", " ");
				annotation = pipeline.process(nounPhrase); 

				List<CoreLabel> tokens = annotation.get(TokensAnnotation.class);
				CoreLabel firstToken = null;
				CoreLabel currToken = null;
				CoreLabel prevToken = null;
				CoreLabel nextToken = null;
				for(int i=0; i<tokens.size();i++){
					if(i==0){
						firstToken = tokens.get(i);
						currToken = firstToken;
					} else {
						currToken = tokens.get(i);
					}
					String currTokenPos = getPosTag(currToken);
					if(currTokenPos.equals("NN") || currTokenPos.equals("NNS") || currTokenPos.equals("JJ")){
						if(hasPrevToken(firstToken,currToken)){
							prevToken = tokens.get(i-1);
							//System.out.println(prevToken.value() + ' ' + prevToken.tag());
							if(prevToken.tag().equals("JJ") ||  prevToken.tag().equals("NN") || prevToken.tag().equals("VBG") || prevToken.tag().equals("NNS")){
								//System.out.println(prevToken.value() + " " + currToken.value());
							} else {
								prevToken = null;
							}
							//System.out.println(currToken.value());
							//System.out.println("has previous token");
						} 

						if(hasNextToken(firstToken,currToken,tokens) && i+1 != tokens.size()) {


							nextToken = tokens.get(i+1);
							if(nextToken.tag().equals("JJ") ||  nextToken.tag().equals("NN") || nextToken.tag().equals("VBG") || nextToken.tag().equals("NNS")){
								//System.out.println(currToken.value() + " " + nextToken.value());

							} else {
								nextToken = null;
							}
							//System.out.println(currToken.value());
							//System.out.println("first token");
						}

						if(checkForNouns(currToken,prevToken,nextToken)){

							String keyword = prevToken.value() + " " + currToken.value() + " " + nextToken.value();
							this.extractedKeywordsList.add(keyword);
							//System.out.println("finalized keyword is 1" + prevToken.value() + " " + currToken.value() + " " + nextToken.value());
						} else if(currToken != null && prevToken == null && nextToken == null){
							//System.out.println(currToken.value());
							//System.out.println(currToken.ner());
							String keyword =  currToken.value();
							this.extractedKeywordsList.add(keyword);
							//System.out.println("finalized keyword is 2"  + currToken.value());
						} else if(currToken != null && prevToken != null && nextToken == null){

							//System.out.println(prevToken.value());
							//System.out.println(prevToken.ner());
							String keyword =  prevToken.value() + " " + currToken.value();
							this.extractedKeywordsList.add(keyword);
							//System.out.println("finalized keyword is 3" + prevToken.value() + " " + currToken.value());
						} else if(currToken != null && prevToken == null && nextToken != null){
							//System.out.println(currToken.value());
							//System.out.println(currToken.ner());
							//System.out.println(nextToken.value());
							//System.out.println(nextToken.ner());
							String keyword =  currToken.value() + " " + nextToken.value();
							this.extractedKeywordsList.add(keyword);
							//System.out.println("finalized keyword is 4" + currToken.value() + " " + nextToken.value());
						}

					} 


				}

			}
			//System.out.println(this.posTaggedNounPhrasesList);
			//System.out.println(this.extractedKeywordsList);
			List<String> refinedList = removeDuplicateKeywords(this.extractedKeywordsList);
			getKeywordsFromNer(refinedList,pipeline);
			getKeywordsFromAlchemy(this.extractedKeywordsList);
			review.setAspectTermsList(this.finalizedAspectTermsList);
			//System.out.println(review.getAspectTermsList());
		}
		//System.out.println(this.posTaggedNounPhrasesList);
	}

	public void getKeywordsFromNer(List<String> list, StanfordCoreNLP pipeline){
		System.out.println("Refined keywords list : " + list);
		Annotation annotation;
		ArrayList<String> nerTaggedList = new ArrayList<String>();
		for(String keyword:list){
			annotation = pipeline.process(keyword);
			String str = "";
			List<CoreLabel> tokens = annotation.get(TokensAnnotation.class);
			for(CoreLabel token: tokens){
				//System.out.println(token.value() + " " + token.ner());
				if(!token.ner().equals("O")){
					str += token.value() + " ";
					nerTaggedList.add(str);
				}
			}
		}

		nerTaggedList = (ArrayList<String>) removeDuplicateKeywords(nerTaggedList);
		this.nerValidatedKeywordsList = nerTaggedList;
		System.out.println("NER validated keywords: " + nerTaggedList);
	}

	public void getKeywordsHttpCaller(String keywordStr) throws IOException{
		//String apiKey = "0906c68965522bfbc4d80d5eb5e096d3481c1cfb";
		String apiKey = "73979c86ca03377a9f74918d052860096fa828bd";
		
		URL urlObj = new URL("http://access.alchemyapi.com/calls/text/TextGetRankedKeywords?apikey=" + apiKey + "&text=" + URLEncoder.encode(keywordStr, "UTF-8") + "&outputMode=json");
		//System.out.println(urlObj.toString() + "\n");

		URLConnection connection = urlObj.openConnection();
		connection.connect();

		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		String line;
		StringBuilder builder = new StringBuilder();
		while ((line = reader.readLine()) !=  null) {
			builder.append(line + "\n");
		}

		//System.out.println(builder.toString());
		Map<String, Double> keywordsList = convertJsonToMap(builder.toString());
		this.alchemyExtractedKeywordsList = keywordsList;
		System.out.println("Alchemy extracted keywords: " + this.alchemyExtractedKeywordsList.keySet());
		this.alchemyExtractedKeywordsList = refineAlchemyKeywords(this.alchemyExtractedKeywordsList);
		ArrayList<String> filteredKeywords = filterKeywords(this.nerValidatedKeywordsList, this.alchemyExtractedKeywordsList);
		//System.out.println(removeDuplicateKeywords(filteredKeywords));
		this.finalizedAspectTermsList = removeDuplicateKeywords(filteredKeywords);
	}

	public Map<String, Double> convertJsonToMap(String jsonStr){

		Gson gson = new Gson();
		Map<String, Double> keywordsList = new HashMap<String,Double>();
		ResponseJSON json = gson.fromJson(jsonStr, ResponseJSON.class);
		//System.out.println(json.language);
		if(json.keywords != null){
			for(Keyword k: json.keywords){
				keywordsList.put(k.text, Double.parseDouble(k.relevance));
				//System.out.println(k.text+":"+k.relevance);
			}
		}
		return keywordsList;
	}

	public void getKeywordsFromAlchemy(ArrayList<String> keywordsList){
		String str = "";
		for(String keyword:keywordsList){
			str+= keyword + ", ";
		}

		try {
			getKeywordsHttpCaller(str);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public Map<String,Double> refineAlchemyKeywords(Map<String,Double> map){
		Iterator<Map.Entry<String, Double>> it;
		for(it = map.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, Double> keyword = it.next();
			if(keyword.getValue() < 0.53) {
				//  System.out.println(keyword.getKey());
				it.remove();
			}
		}
		return map;

	}

	public ArrayList<String> filterKeywords(ArrayList<String> nerKeywordList, Map<String,Double> alchemyList){
		ArrayList<String> filteredKeywords = new ArrayList<String>();
		for(String nerKey:nerKeywordList){
			filteredKeywords.add(nerKey.trim());
		}

		Iterator<Map.Entry<String, Double>> it;
		for(it = alchemyList.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, Double> keyword = it.next();
			filteredKeywords.add(keyword.getKey());
		}
		return filteredKeywords;
	}

	public void writeTofile(String path){

		try {

			System.out.println("Writing to File...");
			this.mapper.writeValue(new File(path), this.reviewObjects);
			System.out.println("Written to file successfully.");
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean checkForNouns(CoreLabel currToken, CoreLabel prevToken, CoreLabel nextToken){
		if(currToken != null && prevToken != null && nextToken != null){
			if(currToken.tag() == "NN" || currToken.tag() == "NNS" || currToken.tag() == "JJ" || currToken.tag().contains("VB")){
				if(prevToken.tag() == "NN" || prevToken.tag() == "NNS" || currToken.tag() =="JJ" || currToken.tag().contains("VB")){
					if(nextToken.tag() == "NN" || nextToken.tag() == "NNS" || currToken.tag() =="JJ" || currToken.tag().contains("VB")){
						return true;
					}
				}

			}
		}
		return false;

	}

	public String getPosTag(CoreLabel token){
		return token.tag().toString();
	}

	public boolean hasPrevToken(CoreLabel firstToken, CoreLabel currToken){
		if(firstToken == currToken){
			return false;
		} else{
			return true;
		}

	}

	public boolean hasNextToken(CoreLabel firstToken, CoreLabel currToken,List<CoreLabel> tokens){
		if(firstToken != currToken && tokens.size() > 2){
			return true;
		} else {
			return false;
		}
	}

	public String removeStopWords(String phrase){

		String stopWordLessPhrase ="";
		String [] stopWords = this.stopWords;
		ArrayList<String> wordList = new ArrayList<String>();
		String [] wordArr = phrase.split(" ");
		for(String word:wordArr){
			wordList.add(word);
		}

		for(int i=0; i<wordList.size();i++){
			for(int j=0; j< stopWords.length;j++){
				//System.out.println("size: "+wordList.size());
				//System.out.println("i: "+ i);
				if(wordList.size() != 0 && wordList.size() == i){
					i--;
				} else if(wordList.size() == 0 && i == 0){
					break;
				}
				if(stopWords[j].toLowerCase().contains(wordList.get(i).toLowerCase())){
					wordList.remove(i);
					
					if(wordList.size() == 1){
						break;
					}
				}

			}
		}

		for(String word: wordList){
			stopWordLessPhrase += word + " ";
		}
		return stopWordLessPhrase;

	}



}
