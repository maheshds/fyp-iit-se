package com.iit.fyp.travelplanner;

import java.util.List;

public class DataPersistenceManager {

	public void saveDataToDatabase(List<TravelReview> reviewObjects){
		PersistenceRDMS persistDataObj = new PersistenceRDMS();
		persistDataObj.persistDataToRDMS(reviewObjects);
		
	}
	
	public void saveToKeywordCache(List<TravelReview> reviewObjects){
		try {
			KeyWordSearch keywordSearchObj = new KeyWordSearch();
			keywordSearchObj.setKeywords(reviewObjects);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
