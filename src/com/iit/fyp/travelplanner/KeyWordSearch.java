package com.iit.fyp.travelplanner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import edu.stanford.nlp.ling.CoreAnnotations.GenericTokensAnnotation;
import redis.clients.jedis.Jedis;

public class KeyWordSearch {
	

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/travel_reviews";
//  Database credentials
	static final String USER = "root";
	static final String PASS = null;
	
	
public void setKeywords(List<TravelReview> reviewObjects) throws ClassNotFoundException{
		Connection conn = null;
		Statement stmt = null;
		boolean sqlStatus = false;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			//STEP 3: Open a connection
			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");
		}catch(SQLException se){
			
		}
		
	      //Connecting to Redis server on localhost
	      Jedis jedis = new Jedis("localhost");
	      System.out.println("Connection to server sucessfully");
	      //check whether server is running or not
	      System.out.println("Server is running: "+jedis.ping());
	      
	      for(TravelReview review: reviewObjects){
	      int attractionId = getAttractionIdByName(conn, stmt, review);
	      jedis.set("location:" + attractionId, review.getAttractionName());
	      for(String aspectTerm: review.getAspectTermsList()){
	      jedis.sadd("keyword:" + aspectTerm, Integer.toString(attractionId));
	      }
	      }
	      System.out.println("Keyword insertion successful.");
	 }
	
	public int getAttractionIdByName(Connection conn, Statement stmt, TravelReview review){
		try {
			stmt = conn.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int attractionId = 0;
		ResultSet rs;
		try {
			rs = stmt.executeQuery(getAttractionId(review));
			while(rs.next()){
				attractionId = rs.getInt("attractionId");
			}
			rs.close();
			return attractionId;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		
	}
	public  String getAttractionId(TravelReview review){
		String sql = "SELECT attractionID FROM attraction WHERE attractionName ='" + review.getAttractionName() + "'";
		return sql;
	}
}
