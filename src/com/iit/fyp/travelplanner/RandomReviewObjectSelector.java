package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class RandomReviewObjectSelector {

	public static void main (String [] args) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper ();
		TravelReview [] reviewObjects = mapper.readValue(new File("c:\\Users\\Mahesh\\Desktop\\fyp\\final-test-data.json"), TravelReview[].class);
		List<TravelReview> fullReviewList = new ArrayList<TravelReview>();
		for(TravelReview review:reviewObjects){
			fullReviewList.add(review);
		}
		System.out.println(fullReviewList.size());
		List<TravelReview> trainList = new ArrayList<TravelReview>();
		for(int i=0; i<150; i++){
			int randomNum = randInt(0,fullReviewList.size());
			TravelReview obj = reviewObjects[randomNum];
			trainList.add(obj);
			fullReviewList.remove(randomNum);
		}
		mapper.writeValue(new File("c:\\Users\\Mahesh\\Desktop\\fyp\\development-data.json"), trainList);
		System.out.println("writing to fyp/NER-test-data - Success!");
		
		mapper.writeValue(new File("c:\\Users\\Mahesh\\Desktop\\fyp\\gold-data.json"), fullReviewList);
		System.out.println("writing to fyp/gold-data-test-data - Success!");
	}
	public static int randInt(int min, int max) {

	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}
