package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CollectionFactory.ArrayListFactory;
import edu.stanford.nlp.util.CoreMap;

public class NERFormatter {
	public static String [] stopWords = {"a","about","above","after","again","against","all","am","an","and","any","are","aren't","as","at","be","because","been","before","being","below","between"
			,"both","but","by","can't","cannot","could","couldn't","did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't","has"
			,"hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself","his","how","how's","i","i'd","i'll","i'm"
			,"i've","if","in","into","is","isn't","it","it's","its","itself","let's","me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only"
			,"or","other","ought","our","ours","ourselves","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some","such","than","that","that's"
			,"the","their","theirs","them","themselves","then","there","there's","these","they","they'd","they'll","they're","they've","this","those","through","to","too","under","until"
			,"up","very","was","wasn't","we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while","who","who's","whom","why"
			,"why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your","yours","yourself","yourselves"};

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		TravelReview [] reviewObjects = mapper.readValue(new File("c:\\Users\\Mahesh\\Desktop\\fyp\\development-data.json"), TravelReview[].class);
		ArrayList<String> iobList = new ArrayList<String>();
		for(TravelReview reviewObj: reviewObjects){
			String review = reviewObj.review;
			ArrayList<String> aspectTerms = aspectTermExtractor(reviewObj.aspectTerms);
			sortSentences(aspectTerms, review,iobList);
		}
		
		//FileWriter writer = new FileWriter("c:\\Users\\Mahesh\\Desktop\\updated-iob.txt");
		FileWriter writer = new FileWriter("c:\\Users\\Mahesh\\Desktop\\fyp\\NER-test-file.txt");
		for(String str: iobList) {
		  writer.write(str);
		}
		writer.close();
		
	}

	public static ArrayList<String> aspectTermExtractor(String sentence){
		ArrayList <String> rawTermsList = new ArrayList<String>();
		ArrayList <String> bracketlessTermsList = new ArrayList<String>();
		ArrayList <String> aspectTermsList = new ArrayList<String>();

		//split string for text between { and } and put it in an array list.
		for (String s : sentence
				.split("\\s(?=\\{)|(?<=\\})\\s")){
		
			rawTermsList.add(s);
		}

		//remove curly braces from the strings.
		for(String term: rawTermsList){
			String s = term.replaceAll("\\{", "").replaceAll("\\}","").replaceAll("\\/", "").replaceAll("\\(", "").replaceAll("\\)","").replaceAll("\\$", "");
			bracketlessTermsList.add(s);
		}


		for(String term:bracketlessTermsList){
			String [] splitArr = term.split("#");
			aspectTermsList.add(splitArr[0]);

		}
		System.out.println(aspectTermsList);
		return aspectTermsList;
	}

	public static void sortSentences(ArrayList<String> termList,String review, ArrayList<String> iobList){

		ArrayList<String> aspectSentences = new ArrayList<String>();
		ArrayList<String> noAspectSentences = new ArrayList<String>();
		Properties props = new Properties(); 
		props.put("annotators", "tokenize, ssplit"); 
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		Annotation annotation;
		//repeat from here..
		annotation = new Annotation(review);
		pipeline.annotate(annotation);
		List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
		if (sentences != null && sentences.size() > 0) {

			for(CoreMap sentence: sentences){
				//System.out.println(sentence);
				//aspectSentences.add(sentence.toString().toLowerCase());
				int termCount =0;
				for(String term:termList){
					if(sentence.toString().toLowerCase().contains(term.toLowerCase())){
					//	System.out.println("has aspect term." + " " + term);
						termCount++;
					} 
				}

				if(termCount == 0){
				//	aspectSentences.add(sentence.toString().toLowerCase());
				} else{
					aspectSentences.add(sentence.toString().toLowerCase());
				}
			}
		}
		Map<String, String> termMapObj = annotateAspectTerms(termList);
		iobFormatter(aspectSentences,termMapObj,iobList);
	}

	public static void iobFormatter(ArrayList<String> aspectSentences,Map<String,String> aspectTermList,ArrayList<String> iobList){
		//ArrayList<String> iobList = new ArrayList<String>();
		for(String sentence:aspectSentences){
		//	ArrayList<String> wordList = new ArrayList<String>();
			//String formattedSentence = sentence.replaceAll("[-+.^:!(),]", "");
			String stopWordRemovedSentence = removeStopWords(sentence, stopWords);
			stopWordRemovedSentence = stopWordRemovedSentence.replaceAll("\\(","").replaceAll("\\)","");
			String [] splitArr = stopWordRemovedSentence.split(" ");
			for(String word: splitArr){
				word = word.replaceAll("\\.","");
				if(!word.equals(" ") && aspectTermList.containsKey(word.toLowerCase())){
					//System.out.println("key exists.");
					String s = word + "\t" + aspectTermList.get(word) +"\n";
					iobList.add(s);
				} else {
					//System.out.println("key doesn't exist.");
					String s = word + "\t" + "O" + "\n";
					iobList.add(s);
				}
			}

		}
		System.out.println(iobList);
	}

	public static Map<String, String> annotateAspectTerms(ArrayList<String> termList){
		Map <String, String> aspectTerms = new HashMap<String, String>();
		//ArrayList<String> aspectTerms2 = new ArrayList<String>();
		for(String term:termList){
			String [] splitWordsArr = term.toString().split(" ");
			System.out.println(splitWordsArr.length);
			if(splitWordsArr.length == 1){
				aspectTerms.put(splitWordsArr[0], "B-TERM");
				//String s = splitWordsArr[0] + "\t" + "B-TERM" +"\n";
				//aspectTerms.put(s);
			} else if (splitWordsArr.length > 1){
				aspectTerms.put(splitWordsArr[0], "B-TERM");
				//String s1 = splitWordsArr[0] + "\t" + "B-TERM"+"\n";
				//aspectTerms.add(s1);
				for(int i=1; i<splitWordsArr.length;i++){
					aspectTerms.put(splitWordsArr[i], "I-TERM");
					//String s2 = splitWordsArr[i] + "\t" + "I-TERM"+"\n";
					//aspectTerms.add(s2);
				}
			}
		}
		System.out.println(aspectTerms);
		return aspectTerms;
	}

	
	public static String removeStopWords(String phrase,String [] stopWordsArr){

		String stopWordLessPhrase ="";
		String [] stopWords = stopWordsArr;
		ArrayList<String> wordList = new ArrayList<String>();
		String [] wordArr = phrase.split(" ");
		for(String word:wordArr){
			wordList.add(word);
		}

		for(int i=0; i<wordList.size();i++){
			for(int j=0; j< stopWords.length;j++){
				//System.out.println("size: "+wordList.size());
				//System.out.println("i: "+ i);
				if(wordList.size() != 0 && wordList.size() == i){
					i--;
				} else if(wordList.size() == 0 && i == 0){
					break;
				}
				if(stopWords[j].toLowerCase().contains(wordList.get(i).toLowerCase())){
					wordList.remove(i);
					
					if(wordList.size() == 1){
						break;
					}
				}

			}
		}

		for(String word: wordList){
			stopWordLessPhrase += word + " ";
		}
		return stopWordLessPhrase;

	}

}
