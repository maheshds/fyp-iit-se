package com.iit.fyp.travelplanner;

import java.awt.List;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class RemoveDuplicates {

	static int counter = 1;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Map<String, ArrayList<TravelReview>> result = new HashMap<>();
			CSVReader reader = new CSVReader(new FileReader(
					"C:\\Users\\Mahesh\\Desktop\\TripAdviser\\Attractions\\Galle\\Top10Attractions4.csv"));

			String[] nextLine;
			reader.readNext();

			while ((nextLine = reader.readNext()) != null) {
				counter++;
				// nextLine[] is an array of values from the line
				if (result.containsKey(nextLine[0].trim())) {
					// compare the review
					ArrayList<TravelReview> arrayList = result.get(nextLine[0].trim());
					//System.out.println(nextLine[0].trim());
					if (reviewNeedsUpdate(arrayList, nextLine[4].trim(), nextLine[2].trim())) {
						//System.out.println("here");
						//System.out.println(nextLine[4].trim());
						TravelReview obj = arrayList.get(duplicateReviewIndex);
						//System.out.println("Previous :" + obj.getReview());
						obj.setReview(nextLine[4].trim());
						System.out.println("After :" + arrayList.get(duplicateReviewIndex).getReview());
						//System.out.println("----------------------------");
						//System.out.println(nextLine[4].trim());
					//	arrayList.set(duplicateReviewIndex, new TravelReview(nextLine)); // update
						// only
						// the
						// review,
						// create
						// a
						// new
						// object,
						// if
						// you
						// like
					} else {
						TravelReview review = new TravelReview(nextLine);
						
						if (!arrayList.contains(review)) {
							System.out.println("ELSE :" + review.getReview());
							arrayList.add(review);
						}

					}
				} else {
					// create TravelRow with array using the constructor eating
					// the line
					
					ArrayList<TravelReview> reviewList = new ArrayList<TravelReview>();
					reviewList.add(new TravelReview(nextLine));
					result.put(nextLine[0].trim(), reviewList);
				}
			}

			//printMap(result);
			String filewrite = "C:\\Users\\Mahesh\\Desktop\\TripAdviser\\Attractions\\Galle\\Galle_nodupes4.csv";
			CSVWriter writer = new CSVWriter(new FileWriter(filewrite));
			String columns = "Organization,Address,Reviewer,Review Title,Review,Review Count,Help Count,Attraction Count,Location,Rating Date,Rating";
			writer.writeNext(columns.split(","));
			printParsedCSV(result,writer);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void printParsedCSV(Map<String, ArrayList<TravelReview>> mp, CSVWriter writer) {
		Iterator<Entry<String, ArrayList<TravelReview>>> it = mp.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			ArrayList<TravelReview> value = (ArrayList<TravelReview>) pair.getValue();
			System.out.println("Size: " + value.size());
			System.out.println("Count " + counter);
			int i = 0;
			for (TravelReview travelReview : value) {

				writer.writeNext(travelReview.toString().split(","));
				i++;

			}
			try {
				writer.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	



	/*
	 * public static void main(String[] args) { // TODO Auto-generated method
	 * stub try { Map<String,TravelReview> result = new
	 * HashMap<String,TravelReview>(); CSVReader reader = new CSVReader(new
	 * FileReader(
	 * "C:\\Users\\Mahesh\\Desktop\\TripAdviser\\Attractions\\Kandy\\merge\\Top10Attractions-merged.csv"
	 * ));
	 * 
	 * String [] nextLine; reader.readNext(); while ((nextLine =
	 * reader.readNext()) != null) { // nextLine[] is an array of values from
	 * the line if( result.containsKey(nextLine[0]) ) { // compare the review
	 * if( reviewNeedsUpdate(result.get(nextLine[0]), nextLine[4]) ) {
	 * result.get(nextLine[0]).setReview(nextLine[4]); // update only the
	 * review, create a new object, if you like } } else { // create TravelRow
	 * with array using the constructor eating the line result.put(nextLine[0],
	 * new TravelReview(nextLine)); } }
	 * 
	 * 
	 * 
	 * for(TravelReview review : result.values()){
	 * System.out.println(review.toString());
	 * 
	 * } } catch (IOException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 */
	private static int duplicateReviewIndex = 0;

	private static boolean reviewNeedsUpdate(ArrayList<TravelReview> rows, String review, String reviewer) {
		duplicateReviewIndex = 0;
		for (TravelReview travelReview : rows) {
			if (travelReview.reviewer.equalsIgnoreCase(reviewer)) {
				
				return (travelReview.review.replaceAll("\\s", "").toLowerCase().endsWith("more"));

			}
			duplicateReviewIndex++;
		}
		duplicateReviewIndex = 0;
		return false;
	}

}
