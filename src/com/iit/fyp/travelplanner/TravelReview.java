package com.iit.fyp.travelplanner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class TravelReview {
	
	UUID reviewId = null;
	String attractionName = null;
	String address = null;
	String reviewer = null;
	String reviewTitle = null;
	String review = null;
	String reviewCount = null;
	String helpCount = null;
	String attractionCount = null;
	String location = null;
	String ratingDate = null;
	String rating = null;
	String aspectTerms = null;
	String aspectCategories = null;
	int reviewLength = 0;
	int aspectCoverage = 0;
	double sceneryAspectScore = 0.0;
	double ambianceAspectScore = 0.0;
	double accommodationAspectScore = 0.0;
	double transportAspectScore = 0.0;
	double entertainmentAspectScore = 0.0;
	double costAspectScore = 0.0;
	double reviewerTrust = 0.0;
	double finalScenaryAspectScore = 0.0;
	double finalAccommodationAspectScore = 0.0;
	double finalAmbianceAspectScore = 0.0;
	double finalEntertainmentAspectScore = 0.0;
	double finalCostAspectScore = 0.0;
	
	//ArrayList <String> segmentsArr = null;
	//ArrayList <String> posTaggedArr = null;
	ArrayList <String> nounPhrasesList = null;
	ArrayList <String> verbPhrasesList = null;
	List <String> aspectTermsList = null;
	List <String> aspectCategoriesList = null;
	Map<String, String> aspectTermAndAdjectivesMap = null;
	Map<String,Double> aspectTermsAndSentimentScoresMap = null;


	public TravelReview(String[] row) {
		
		this.reviewId = UUID.randomUUID();
		this.attractionName = row[0].trim();
		this.address = row[1].trim();
		this.reviewer = row[2].trim();
		this.reviewTitle = row[3].trim();
		this.review = row[4].trim();
		this.reviewCount = row[5].trim();
		this.helpCount = row[6].trim();
		this.attractionCount = row[7].trim();
		this.location = row[8].trim();
		this.ratingDate = row[9].trim();
		this.rating = row[10].trim();
		//this.aspectTerms = row[12].trim();
		//this.aspectCategories = row[13].trim();
		
	

	}
	
	public TravelReview() {
	
	}
	
	@Override
	public String toString() {
		return new StringBuilder(reviewId.toString()).append(",")
				.append(attractionName)
				.append(",")
				.append(address)
				.append(",")
				.append(reviewer)
				.append(",")
				.append(reviewTitle)
				.append(",")
				.append(review)
				.append(",")
				.append(reviewCount)
				.append(",")
				.append(helpCount)
				.append(",")
				.append(attractionCount)
				.append(",")
				.append(location)
				.append(",")
				.append(ratingDate)
				.append(",")
				.append(rating)
				.append(",")
				.append(aspectTerms)
				.append(",")
				.append(aspectCategories)
				.append(",")
				.append(nounPhrasesList)
				.append(",")
				.append(verbPhrasesList)
				.append(",")
				.append(aspectTermsList)
				.toString();
				
	}
	
	public String getReviewId() {
		return reviewId.toString();
	}

	public void setReviewId(UUID uuid) {
		this.reviewId = uuid;
	}

	public String getAttractionName() {
		return attractionName;
	}

	public void setAttractionName(String attractionName) {
		this.attractionName = attractionName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public String getReviewTitle() {
		return reviewTitle;
	}

	public void setReviewTitle(String reviewTitle) {
		this.reviewTitle = reviewTitle;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(String reviewCount) {
		this.reviewCount = reviewCount;
	}

	public String getHelpCount() {
		return helpCount;
	}

	public void setHelpCount(String helpCount) {
		this.helpCount = helpCount;
	}

	public String getAttractionCount() {
		return attractionCount;
	}

	public void setAttractionCount(String attractionCount) {
		this.attractionCount = attractionCount;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRatingDate() {
		return ratingDate;
	}

	public void setRatingDate(String ratingDate) {
		this.ratingDate = ratingDate;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getAspectTerms() {
		return aspectTerms;
	}

	public void setAspectTerms(String aspectTerms) {
		this.aspectTerms = aspectTerms;
	}

	public String getAspectCategories() {
		return aspectCategories;
	}

	public void setAspectCategories(String aspectCategories) {
		this.aspectCategories = aspectCategories;
	}

//	public ArrayList<String> getSegmentsArr() {
//		return segmentsArr;
//	}
//
//	public void setSegmentsArr(ArrayList<String> segmentsArr) {
//		this.segmentsArr = segmentsArr;
//	}
//
//	public ArrayList<String> getPosTaggedArr() {
//		return posTaggedArr;
//	}
//
//	public void setPosTaggedArr(ArrayList<String> posTaggedArr) {
//		this.posTaggedArr = posTaggedArr;
//	}

	public int getReviewLength() {
		return reviewLength;
	}

	public void setReviewLength(int reviewLength) {
		this.reviewLength = reviewLength;
	}

	public int getAspectCoverage() {
		return aspectCoverage;
	}

	public void setAspectCoverage(int aspectCoverage) {
		this.aspectCoverage = aspectCoverage;
	}

	public double getSceneryAspectScore() {
		return sceneryAspectScore;
	}

	public void setSceneryAspectScore(double sceneryAspectScore) {
		this.sceneryAspectScore = sceneryAspectScore;
	}

	public double getAmbianceAspectScore() {
		return ambianceAspectScore;
	}

	public void setAmbianceAspectScore(double ambianeAspectScore) {
		this.ambianceAspectScore = ambianeAspectScore;
	}

	public double getAccommodationAspectScore() {
		return accommodationAspectScore;
	}

	public void setAccommodationAspectScore(double accommodationAspectScore) {
		this.accommodationAspectScore = accommodationAspectScore;
	}

	public double getTransportAspectScore() {
		return transportAspectScore;
	}

	public void setTransportAspectScore(double transportAspectScore) {
		this.transportAspectScore = transportAspectScore;
	}

	public double getEntertainmentAspectScore() {
		return entertainmentAspectScore;
	}

	public void setEntertainmentAspectScore(double entertainmentAspectScore) {
		this.entertainmentAspectScore = entertainmentAspectScore;
	}

	public double getCostAspectScore() {
		return costAspectScore;
	}

	public void setCostAspectScore(double costAspectScore) {
		this.costAspectScore = costAspectScore;
	}

	public double getReviewerTrust() {
		return reviewerTrust;
	}

	public void setReviewerTrust(double reviewerTrust) {
		this.reviewerTrust = reviewerTrust;
	}

	public double getFinalScenaryAspectScore() {
		return finalScenaryAspectScore;
	}

	public void setFinalScenaryAspectScore(double finalScenaryAspectScore) {
		this.finalScenaryAspectScore = finalScenaryAspectScore;
	}

	public double getFinalAccommodationAspectScore() {
		return finalAccommodationAspectScore;
	}

	public void setFinalAccommodationAspectScore(double finalAccommodationAspectScore) {
		this.finalAccommodationAspectScore = finalAccommodationAspectScore;
	}

	public double getFinalAmbianceAspectScore() {
		return finalAmbianceAspectScore;
	}

	public void setFinalAmbianceAspectScore(double finalAmbianceAspectScore) {
		this.finalAmbianceAspectScore = finalAmbianceAspectScore;
	}

	public double getFinalEntertainmentAspectScore() {
		return finalEntertainmentAspectScore;
	}

	public void setFinalEntertainmentAspectScore(double finalEntertainmentAspectScore) {
		this.finalEntertainmentAspectScore = finalEntertainmentAspectScore;
	}

	public double getFinalCostAspectScore() {
		return finalCostAspectScore;
	}

	public void setFinalCostAspectScore(double finalCostAspectScore) {
		this.finalCostAspectScore = finalCostAspectScore;
	}

	public ArrayList<String> getNounPhrasesList() {
		return nounPhrasesList;
	}

	public void setNounPhrasesList(ArrayList<String> nounPhrasesList) {
		this.nounPhrasesList = nounPhrasesList;
	}

	public ArrayList<String> getVerbPhrasesList() {
		return verbPhrasesList;
	}

	public void setVerbPhrasesList(ArrayList<String> verbPhrasesList) {
		this.verbPhrasesList = verbPhrasesList;
	}

	public List<String> getAspectTermsList() {
		return aspectTermsList;
	}

	public void setAspectTermsList(List<String> finalizedAspectTermsList) {
		this.aspectTermsList = finalizedAspectTermsList;
	}

	public List<String> getAspectCategoriesList() {
		return aspectCategoriesList;
	}

	public void setAspectCategoriesList(List<String> aspectCategoriesList) {
		this.aspectCategoriesList = aspectCategoriesList;
	}

	public Map<String, String> getAspectTermAndAdjectivesMap() {
		return aspectTermAndAdjectivesMap;
	}

	public void setAspectTermAndAdjectivesMap(Map<String, String> map) {
		this.aspectTermAndAdjectivesMap = map;
	}

	public Map<String, Double> getAspectTermsAndSentimentScoresMap() {
		return aspectTermsAndSentimentScoresMap;
	}

	public void setAspectTermsAndSentimentScoresMap(Map<String, Double> aspectTermsAndSentimentScoresMap) {
		this.aspectTermsAndSentimentScoresMap = aspectTermsAndSentimentScoresMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ratingDate == null) ? 0 : ratingDate.hashCode());
		result = prime * result + ((reviewTitle == null) ? 0 : reviewTitle.hashCode());
		result = prime * result + ((reviewer == null) ? 0 : reviewer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TravelReview other = (TravelReview) obj;
		if (ratingDate == null) {
			if (other.ratingDate != null)
				return false;
		} else if (!ratingDate.equals(other.ratingDate))
			return false;
		if (reviewTitle == null) {
			if (other.reviewTitle != null)
				return false;
		} else if (!reviewTitle.equals(other.reviewTitle))
			return false;
		if (reviewer == null) {
			if (other.reviewer != null)
				return false;
		} else if (!reviewer.equals(other.reviewer))
			return false;
		return true;
	}

	

}
