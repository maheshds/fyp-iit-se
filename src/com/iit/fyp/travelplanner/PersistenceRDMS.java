package com.iit.fyp.travelplanner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


public class PersistenceRDMS {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/travel_reviews";
	//define categoryIds
	static final int scenaryCatId = 1;
	static final int accommodationCatId = 2;
	static final int ambianceCatId = 3;
	static final int entertainmentCatId = 4;
	static final int costCatId = 5;

	//  Database credentials
	static final String USER = "root";
	static final String PASS = null;
	
	public void persistDataToRDMS(List<TravelReview> reviewObjects){

		Connection conn = null;
		Statement stmt = null;
		boolean sqlStatus = false;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			//STEP 3: Open a connection
			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.setAutoCommit(false); //transaction block started.
			System.out.println("Connected database successfully...");
			
			//INSERT into attraction, reviewer, review, aspect_terms
			for(TravelReview review: reviewObjects){
				System.out.println("here");
			sqlStatus = executeSQLOperation(reviewObjects, conn, stmt,review, 1);
			if(sqlStatus){
				System.out.println("SUCCESS: attraction, reviewer, review and aspect_terms data persistence completed...");
			} else {
				System.out.println("ERROR: attraction, reviewer, review and aspect_terms data persistence failed...");
			}
			
			//INSERT into review_aspect_term
			sqlStatus = executeSQLOperation(reviewObjects, conn, stmt,review, 2);
			if(sqlStatus){
				System.out.println("SUCCESS: review_aspect_term data persistence completed...");
			} else {
				System.out.println("ERROR: review_aspect_term data persistence failed...");
			}
			
			//INSERT into review_aspect_category
			sqlStatus = executeSQLOperation(reviewObjects, conn, stmt,review, 3);
			if(sqlStatus){
				System.out.println("SUCCESS: review_aspect_category data persistence completed...");
			} else {
				System.out.println("ERROR: review_aspect_category data persistence failed...");
			}
			}
			conn.commit(); //transaction block ended.

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		System.out.println("Review Data Persistence Completed!");
	}
	
	public boolean executeSQLOperation(List<TravelReview> reviewObjects,Connection conn, Statement stmt,TravelReview review, int value) throws SQLException{
		
			stmt = conn.createStatement ();
			switch(value){
			case 1:
				insertReviewsIntoDatabase(conn,stmt, review);
				return true;
			case 2:
				insertIntoReviewAspectTermTable(conn, stmt, review);
				return true;
			case 3:
				insertIntoReviewAspectCategoryTable(conn, stmt, review);
				return true;
			default:
				return false;
			}
		
		
	}

	public  void insertReviewsIntoDatabase(Connection conn,Statement stmt, TravelReview review){
		//STEP 1: INSERT into attraction
		System.out.println("Inserting into Attraction table...");
		try {
			int i = stmt.executeUpdate(getAttractionInsertQueryStatement(review));

			System.out.println("Successfully inserted into attraction table.");
			System.out.println("Obtaining attractionId...");
			int attractionId = 0;
			ResultSet rs = stmt.executeQuery(getAttractionId(review));
			while(rs.next()){
				attractionId = rs.getInt("attractionId");
			}
			rs.close();

			if(attractionId != 0){
				System.out.println("attractionId is: " + attractionId);	
			}

			//STEP 2: INSERT into reviewer
			System.out.println("Inserting into reviewer table...");
			int j = stmt.executeUpdate(getReviewerInsertQueryStatement(review));
			System.out.println("Successfully inserted into reviewer table.");
			int reviewerId = 0;
			rs = stmt.executeQuery(getReviewerId(review));
			while(rs.next()){
				reviewerId = rs.getInt("reviewerId");
			}
			rs.close();

			if(reviewerId != 0){
				System.out.println("reviewerId is: " + reviewerId);	
			}

			//STEP 3: INSERT into review
			System.out.println("Inserting into review table...");
			if(attractionId != 0 && reviewerId != 0){
				PreparedStatement ps = getReviewInsertQueryStatement(review,conn, attractionId, reviewerId);
				int reviewInsertionFlag = ps.executeUpdate();

				System.out.println(reviewInsertionFlag);
				if(reviewInsertionFlag == 1){
					System.out.println("Successfully inserted into review table.");
				} else{
					System.out.println("ERROR: Insertion to review table failed.");
				}

			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		//STEP 4: INSERT aspect terms
		for(String aspectTerm: review.getAspectTermsList()){
			System.out.println("Inserting into aspect_terms table...");
			PreparedStatement psAspectTerm = getAspectTermInsertQueryStatement(aspectTerm, conn);
			try {
				int k = psAspectTerm.executeUpdate();
				int m = stmt.executeUpdate("ALTER TABLE aspect_terms AUTO_INCREMENT = 1"); //auto-inncrement
				if(k == 1){
					System.out.println("Successfully inserted into aspect_terms table..");
				} else {
					System.out.println("ERROR: Insertion to aspect_terms failed...");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public  void insertIntoReviewAspectTermTable(Connection conn, Statement stmt, TravelReview review){
		for(String aspectTerm: review.getAspectTermsList()){
			//STEP 5: INSERT into review_aspect_term

			//5.1 Get aspectTermId
			int aspectTermId = 0;
			ResultSet rs;
			try {
				rs = stmt.executeQuery(getAspectTermId(aspectTerm));
				while(rs.next()){
					aspectTermId = rs.getInt("aspectTermId");
				}
				rs.close();
				System.out.println("aspectTermId: " + aspectTermId);

				//get reviewId
				String reviewId ="";
				PreparedStatement ps = getReviewId(conn, review);
				rs = ps.executeQuery();
				while(rs.next()){
					reviewId = rs.getString("reviewId");
				}
				rs.close();
				System.out.println("reviewId: " + reviewId);

				//5.2 INSERT to review_aspect_term table.
				PreparedStatement psReviewAspectTerm = getReviewAspectTermInsertQueryStatement(conn, review, aspectTermId, reviewId);
				System.out.println(psReviewAspectTerm);
				int l = psReviewAspectTerm.executeUpdate();

				if(l == 1){
					System.out.println("Successfully inserted into review_aspect_term table..");
				} else {
					System.out.println("ERROR: Insertion to review_aspect_term failed...");
				}

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}



		}
	}

	public  void insertIntoReviewAspectCategoryTable(Connection conn, Statement stmt, TravelReview review){

		//Get reviewId
		ResultSet rs;
		String reviewId ="";
		PreparedStatement ps = getReviewId(conn, review);
		try {
			rs = ps.executeQuery();
			while(rs.next()){
				reviewId = rs.getString("reviewId");
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//insert scores.
		
		try {
			PreparedStatement psReviewAspectCategory;
			psReviewAspectCategory = getReviewAspectCategoryInsertQueryStatement(conn, review, scenaryCatId, reviewId, (float) review.getFinalScenaryAspectScore() );
			int scenaryRes = psReviewAspectCategory.executeUpdate();
			printQueryStatus("Scenary", scenaryRes);
			
			psReviewAspectCategory = getReviewAspectCategoryInsertQueryStatement(conn, review, accommodationCatId, reviewId, (float) review.getFinalAccommodationAspectScore() );
			int accommodationRes = psReviewAspectCategory.executeUpdate();
			printQueryStatus("Accommodation", accommodationRes);
			
			psReviewAspectCategory = getReviewAspectCategoryInsertQueryStatement(conn, review, ambianceCatId, reviewId, (float) review.getFinalAmbianceAspectScore() );
			int ambianceRes = psReviewAspectCategory.executeUpdate();
			printQueryStatus("Ambiance", ambianceRes);
			
			psReviewAspectCategory = getReviewAspectCategoryInsertQueryStatement(conn, review, entertainmentCatId, reviewId, (float) review.getFinalEntertainmentAspectScore() );
			int entertainmentRes = psReviewAspectCategory.executeUpdate();
			printQueryStatus("Entertainment", entertainmentRes);
			
			psReviewAspectCategory = getReviewAspectCategoryInsertQueryStatement(conn, review, costCatId, reviewId, (float) review.getFinalCostAspectScore() );
			int costRes = psReviewAspectCategory.executeUpdate();
			printQueryStatus("Cost", costRes);
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  void printQueryStatus(String name, int val){
		if(val == 1){
			System.out.println("Insertion of " + name + " is successful");
		} else {
			System.out.println("Insertion of " + name + " is successful");
		}
	}

	public  PreparedStatement getReviewId(Connection conn, TravelReview review){
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement("SELECT reviewId FROM review WHERE review =?");
			ps.setString(1, review.getReview());
			return ps;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public  PreparedStatement getCategoryId(Connection conn, String categoryName){
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement("SELECT categoryId FROM categories WHERE categoryName =?");
			ps.setString(1, categoryName);
			return ps;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public  String getAttractionId(TravelReview review){
		String sql = "SELECT attractionID FROM attraction WHERE attractionName ='" + review.getAttractionName() + "'";
		return sql;
	}

	public  String getReviewerId(TravelReview review){
		String sql = "SELECT reviewerId FROM reviewer WHERE reviewerName = '" + review.getReviewer() + "'";
		return sql;
	}

	public  String getAspectTermId(String aspectTerm){
		String sql = "SELECT aspectTermId FROM aspect_terms WHERE aspectTerm = '" + aspectTerm + "'";
		return sql;
	}


	public  String getAttractionInsertQueryStatement(TravelReview review){
		String sql ="INSERT IGNORE INTO attraction(attractionId, attractionName, address)" +
				"VALUES (" + null + ", '" + review.getAttractionName() + "', '"+ review.getAddress() + "')";

		return sql;

	}

	public  String getReviewerInsertQueryStatement(TravelReview review){
		String sql ="INSERT IGNORE INTO reviewer " +
				"VALUES (" + null + ", '" + review.getReviewer() + "', '"+ review.getLocation() + "')";

		return sql;
	}

	public PreparedStatement getReviewInsertQueryStatement(TravelReview review,Connection conn, int attractionId, int reviewerId){


		try {
			PreparedStatement ps = conn.prepareStatement("INSERT INTO review VALUES (?,?,?,?,?,?,?)");
			ps.setString(1, review.getReviewId());
			ps.setString(2, review.getReviewTitle());
			ps.setString(3, review.getReview());
			ps.setString(4, review.getRatingDate());
			ps.setFloat(5, 0.0f);
			ps.setInt(6, attractionId);
			ps.setInt(7, reviewerId);

			return ps;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}


	public PreparedStatement getAspectTermInsertQueryStatement(String aspectTerm,Connection conn){

		try {
			PreparedStatement ps = conn.prepareStatement("INSERT IGNORE INTO aspect_terms VALUES (?,?)");
			ps.setString(1, "");
			ps.setString(2, aspectTerm);
			return ps;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}


	}

	public PreparedStatement getReviewAspectTermInsertQueryStatement(Connection conn, TravelReview review, int aspectTermId, String reviewId){
		Float aspectTermSentimentScore = 0.0f;

		try {
			PreparedStatement psReviewAspectTerm = conn.prepareStatement("INSERT INTO review_aspect_term VALUES (?,?,?)");
			psReviewAspectTerm.setFloat(1, aspectTermSentimentScore);
			psReviewAspectTerm.setInt(2, aspectTermId);
			psReviewAspectTerm.setString(3, reviewId);
			return psReviewAspectTerm;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public PreparedStatement getReviewAspectCategoryInsertQueryStatement(Connection conn, TravelReview review, int categoryId, String reviewId, Float categorySentimentScore){

		try {
			PreparedStatement psReviewAspectCategory = conn.prepareStatement("INSERT INTO review_aspect_category VALUES (?,?,?)");
			psReviewAspectCategory.setFloat(1, categorySentimentScore);
			psReviewAspectCategory.setInt(2, categoryId);
			psReviewAspectCategory.setString(3, reviewId);
			return psReviewAspectCategory;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
}
