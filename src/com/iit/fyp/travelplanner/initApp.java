package com.iit.fyp.travelplanner;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opencsv.CSVReader;

public class initApp {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {
		
		//create CSV reader and ObjectMapper objects.
		CSVReader reader = new CSVReader(new FileReader(
				"C:\\Users\\Mahesh\\Desktop\\test-kandy.csv"));
		ObjectMapper mapper = new ObjectMapper();
		List<TravelReview> reviewObjects = new ArrayList<TravelReview>();
		
		/* NLP module */
		ReviewAnalyzerEngine analyzerEngineObj = new ReviewAnalyzerEngine();
		reviewObjects = analyzerEngineObj.analyzeReviews(reader, mapper, reviewObjects);
		
		
		/* Fuzzy logic module */
		WeightAndSentimentAllocator weightAndSentimentObj = new WeightAndSentimentAllocator();
		for(TravelReview review:reviewObjects){
			weightAndSentimentObj.allocateWeightsAndSentiments(reviewObjects, review);
		}
		
		
		/* Data persistence module */
		DataPersistenceManager persistenceManagerObj = new DataPersistenceManager();
		//persistenceManagerObj.saveDataToDatabase(reviewObjects);
		//persistenceManagerObj.saveToKeywordCache(reviewObjects);
		
		
		
	}

}
