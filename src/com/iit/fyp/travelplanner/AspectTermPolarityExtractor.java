package com.iit.fyp.travelplanner;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opencsv.CSVReader;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class AspectTermPolarityExtractor {

	Map<String, String> aspectTermAndAdjectivesMap;
	Map<String,Double> aspectTermsAndSentimentScoresMap;
	static List<SentiWordNet> sentiWordNetArr;
	static SWN3 sentiWordNet;
	
	public AspectTermPolarityExtractor(){

		sentiWordNet = new SWN3();
	}

	public  Map<String, String> extractAdjectives(TravelReview  review, StanfordCoreNLP pipeline, Annotation annotation){
		String prepReviewStr = review.review.replaceAll("\\.", "\\. ");
		annotation = new Annotation(prepReviewStr);
		pipeline.annotate(annotation);
		List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
		List<String> aspectTermsList = review.getAspectTermsList();
		//{"a", "b","c"}
		for(String aspectTerm: aspectTermsList){
			//System.out.println(aspectTerm);
			Map<String, List<String>> adjectives = new HashMap<String, List<String>>();
			for(CoreMap sentence:sentences){
				if(isContain(sentence.toString(),aspectTerm)){
					//sentence has aspectTerm.
					String preProcessedStr = sentence.toString().replaceAll("  ", " ");
					//System.out.println(preProcessedStr);
					String[] str = preProcessedStr.split(aspectTerm);
					if(str.length == 0)
						break;
					//System.out.println(str.length);
					String beforeAspectTerm = str[0];
					String afterAspectTerm = null;
					if(str.length != 1){
						afterAspectTerm = str[1];
					} 

					checkForAdjectives(aspectTerm,adjectives,beforeAspectTerm,afterAspectTerm,pipeline, annotation);

				}
			}
		}
		return aspectTermAndAdjectivesMap;
	}

	private  boolean isContain(String source, String subItem){
	
		String pattern = "\\b"+subItem+"\\b";
		Pattern p=Pattern.compile(pattern);
		Matcher m=p.matcher(source);
		return m.find();
	}

	public  void checkForAdjectives(String aspectTerm, Map<String, List<String>> adjectives , String before, String after,StanfordCoreNLP pipeline, Annotation annotation){


		List<String> obj1;
		List<String> obj2 = null;

		obj1 =	getBeforeAspectTermAdjectives(aspectTerm,adjectives,before,pipeline, annotation);

		if(after != null){

			obj2 =	getAfterAspectTermAdjectives(aspectTerm,adjectives,after,pipeline, annotation);
		}
		if(obj1 !=null && !obj1.isEmpty()){
			String adj = "";
			for(String obj: obj1){
				adj += obj + " "; 
			}
			aspectTermAndAdjectivesMap.put(aspectTerm,adj);
		}

		if(obj2 != null && !obj2.isEmpty()){
			String adj = "";
			for(String obj: obj1){
				adj += obj + " "; 
			}
			aspectTermAndAdjectivesMap.put(aspectTerm,adj);
		}

		//aspectTermAndAdjectivesMap.entrySet().removeIf(e-> <boolean expression> );

	}

	public  List<String> getBeforeAspectTermAdjectives(String aspectTerm,Map<String, List<String>> adjectives, String str, StanfordCoreNLP pipeline, Annotation annotation){
		String [] temp = str.split(" ");
		List<String> listObj = new ArrayList<String>();
		int tempArrLength = temp.length;
		int k = 0;
		if(tempArrLength > 1){ //check if at least two or more words are there.
			k = 2;
		} else if(tempArrLength == 1){
			k = 1;
		}
		if(tempArrLength > 0){ 
			for(int i=k; i>0; i--){
				annotation = new Annotation(temp[tempArrLength - i]);
				pipeline.annotate(annotation);
				List<CoreLabel> tokens = annotation.get(TokensAnnotation.class);
				for(CoreLabel token:tokens){
					//System.out.println(token.value() + " " + token.tag());
					if(token.tag().equals("JJ") || token.tag().equals("RB")){
						//System.out.println(aspectTerm + " " +token.value() +" " +token.tag());
						//listObj.add(token.value() +"," +token.tag());
						listObj.add(token.value());
						//adjectives.put(aspectTerm, listObj);
					} 
				}
			}
		}
		return listObj;
	}

	public  List<String> getAfterAspectTermAdjectives(String aspectTerm,Map<String, List<String>> adjectives, String str, StanfordCoreNLP pipeline, Annotation annotation){

		String [] temp = str.split(" ");
		List<String> listObj = new ArrayList<String>();
		int tempArrLength = temp.length;
		int k = 0;
		if(tempArrLength > 1){ //check if at least two or more words are there.
			k = 2;
		} else if(tempArrLength == 1){
			k = 1;
		}

		if(tempArrLength != 0){
			for(int i=0; i<k; i++){
				annotation = new Annotation(temp[i]);
				pipeline.annotate(annotation);
				List<CoreLabel> tokens = annotation.get(TokensAnnotation.class);
				for(CoreLabel token:tokens){
					//System.out.println(token.value() + " " + token.tag());
					if(token.tag().equals("JJ") || token.tag().equals("RB")){
						//System.out.println(aspectTerm + " " +token.value() +" " +token.tag());
					//	listObj.add(token.value() +"," +token.tag());
						listObj.add(token.value());
						//adjectives.put(aspectTerm, listObj);

					} else {
						return null;
					}
				}

			}
		}
		return listObj;
	}

	

	public  double getSentimentScore(String word){
		double totalScore =0.0;
		word = word.replaceAll("([^a-zA-Z\\s])", "");
		return totalScore += sentiWordNet.extract(word);
	}
	
	public  void processAspectTermsAndAdjectives(List<String> aspectTermsList,Map<String, String> adjectives ){
	Iterator<Entry<String, String>> it;
	List<String> tempList = aspectTermsList;
	
	for(it = adjectives.entrySet().iterator(); it.hasNext(); ) {
		Map.Entry<String, String> adjectiveObj = it.next();
		 String temp = adjectiveObj.getKey().toLowerCase() + " " + adjectiveObj.getValue().toLowerCase();
		 getOverallSentimentScore(adjectiveObj.getKey().toLowerCase(),temp);
		 tempList.remove(adjectiveObj.getKey());
	}
	
	for(String aspectTerm:tempList){
		String temp = aspectTerm.toLowerCase();
		getOverallSentimentScore(aspectTerm,temp);
	}

	}
	
	public  void getOverallSentimentScore(String aspectTerm, String appendedStr){
		//System.out.println(appendedStr);
		double sentimentScore = 0.0;
		String [] wordArr = appendedStr.split("\\s+");
		for(String word:wordArr){
		//	System.out.println(word);
			double sentiWordScore = getSentimentScore(word);
			sentimentScore = sentimentScore + sentiWordScore;

		}
		if(appendedStr.contains("not ") || appendedStr.contains(" not")){
			sentimentScore = -(sentimentScore);
		}
		
		sentimentScore  = ((sentimentScore - -1) / (1 - -1)) * (10 - 1) + 1;
		aspectTermsAndSentimentScoresMap.put(aspectTerm, sentimentScore);
	}

	
}
