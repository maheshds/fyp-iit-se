package com.iit.fyp.travelplanner;
import java.util.*; 
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.ling.*; 
import edu.stanford.nlp.ling.CoreAnnotations.*; 

public class LemmaDemo {

 

	
	    public static void main(String[] args)
	    {
	        Properties props = new Properties(); 
	        props.put("annotators", "tokenize, ssplit, pos, lemma"); 
	        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	        String text = "the security personnel at the entrance gate were very co operative as my child was in a stroller. SAARC countries have a discounted entry fee. again there was proper arrangement by lift for the baby stroller.the premises are huge and the garden well maintained. the sanctum sanctorum was full of people but surprisingly there was no pushing or pulling. i could soak in the atmosphere at my own pace. the tooth of Gautam Budha can not be seen as it is covered by a gold crown about 5 ft high.check for the timings when one can see the sanctum sanctorum"; 
	        Annotation document = pipeline.process(text);  

	        for(CoreMap sentence: document.get(SentencesAnnotation.class))
	        {    
	        	String sentenceStr ="";
	            for(CoreLabel token: sentence.get(TokensAnnotation.class))
	            {       
	                
	                String lemma = token.get(LemmaAnnotation.class); 
	                String pos = token.get(PartOfSpeechAnnotation.class);
	                sentenceStr += lemma  + "(" + pos + ") ";
	                
	                
	            }
	          System.out.println("parsed version :" + sentenceStr);
	        }
	    }
	}


