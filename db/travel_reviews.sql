-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2016 at 06:11 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel_reviews`
--
CREATE DATABASE IF NOT EXISTS `travel_reviews` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `travel_reviews`;

-- --------------------------------------------------------

--
-- Table structure for table `aspect_terms`
--

CREATE TABLE `aspect_terms` (
  `aspectTermId` int(11) NOT NULL,
  `aspectTerm` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aspect_terms`
--

INSERT INTO `aspect_terms` (`aspectTermId`, `aspectTerm`) VALUES
(16, 'budhha'),
(22, 'camera'),
(19, 'climb'),
(13, 'crowds'),
(6, 'devotees'),
(20, 'different postures'),
(7, 'exception'),
(8, 'fervor'),
(15, 'Great temple'),
(24, 'guide'),
(3, 'high fee'),
(9, 'home power'),
(23, 'importance history'),
(5, 'museum'),
(4, 'museums exhibitions'),
(18, 'outer rooms'),
(12, 'peak'),
(14, 'piece'),
(2, 'ranks'),
(21, 'shoes'),
(1, 'special place'),
(17, 'time'),
(11, 'timings'),
(10, 'Visits');

-- --------------------------------------------------------

--
-- Table structure for table `attraction`
--

CREATE TABLE `attraction` (
  `attractionId` int(11) NOT NULL,
  `attractionName` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attraction`
--

INSERT INTO `attraction` (`attractionId`, `attractionName`, `address`) VALUES
(1, 'Temple of the Tooth (Sri Dalada Maligawa)', 'Address: Sri Dalada Veediya Kandy 20000 Sri Lanka');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(15) NOT NULL,
  `categoryName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryId`, `categoryName`) VALUES
(1, 'SCENARY'),
(2, 'ACCOMMODATION'),
(3, 'AMBIANCE'),
(4, 'ENTERTAINMENT'),
(5, 'COST');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `reviewId` varchar(35) NOT NULL,
  `title` varchar(100) NOT NULL,
  `review` varchar(2000) NOT NULL,
  `date` varchar(35) NOT NULL,
  `reviewOverallScore` double NOT NULL,
  `attractionId` int(11) NOT NULL,
  `reviewerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`reviewId`, `title`, `review`, `date`, `reviewOverallScore`, `attractionId`, `reviewerId`) VALUES
('0ee30902-3736-4cd3-86b9-dc0e8d74210', 'Very good and peace', 'It''s very nice at the top with different postures of Lord Buddha. The climb to the temple is daunting so do go with your shoes on', '2 days ago', 0, 1, 8),
('16e04410-ce59-4390-9cf3-8e7f10feb82', 'Interesting experience', 'Set in beautiful gardens and by the lake  this large temple complex is worth a visit! There are several smaller museums and exhibitions within the complex  all worthwhile!', 'yesterday', 0, 1, 3),
('2160fa1a-9818-4163-87ed-3dd5d718fa7', 'Temple tour', 'Visits to places of worship always bring home to me the power of superstition. The Temple of the Tooth was no exception. But I couldn''t help but marvel at the fervor with which some devotees were praying. One tip though: the shrine that houses the Tooth  is open only twice a day and so it''s best to check these timings  though I would imagine that the crowds would be at a peak.', '2 days ago', 0, 1, 5),
('2fdad21d-9941-46b3-acf7-fbdb3ae5a17', 'Sacred place', 'This is the place you must visit. Kandy special place. Generaly if you have traveled in Asia much  you will not find anything new  exept nice temple and sanctuary of budhism. They did not allow people with shorts  short skirts  etc. if you go think about this before ;)', 'yesterday', 0, 1, 1),
('3f4eab40-359f-47b3-837d-a0176207528', 'Interesting history', 'Worth a visit do the museum as well as the Temple. Go dressed appropriately ladies long skirts or trousers men trousers.', 'yesterday', 0, 1, 4),
('6a86b7e5-fa15-489b-8751-de75a64512c', 'Nice but full of tourists', 'Definetly not the quite place i was expecting it to be.... They charge extra to bring the camera in.', '3 days ago', 0, 1, 9),
('6c5369ed-ec78-40c1-ad6a-5f2623cdc76', 'Drab temple', 'I don''t understand why this temple is so well liked  I have see many temples across the world. This one ranks near the bottom  I would not recommend going out of your way to see it let alone pay a high fee to get in.', 'yesterday', 0, 1, 2),
('879f68e1-5711-4192-8445-7856ac9bcac', 'Nice visit', 'This is an important temple for the Buddhist. It is very crowded but very nice. You will get pushed around if you go near the time of opening of the relic for viewing. But very nice temple and a must see even if you are not a Buddhist.', '2 days ago', 0, 1, 7),
('92c3df0e-b02d-483f-89dd-77252238629', 'Crowded and crazy cultural highlight.', 'Great temple  with a piece of the budhha inside. Take some time to explore the outer rooms and artifacts.', '2 days ago', 0, 1, 6),
('bac13b39-bf16-49ed-b498-d02ebddbbf7', 'Average.', 'We had a guide to ensure we tried to understand the importance and history  however for me it was highly underrated. Go past and walk around  read the history  and see the temple  however don''t expect too much.', '3 days ago', 0, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `reviewer`
--

CREATE TABLE `reviewer` (
  `reviewerId` int(11) NOT NULL,
  `reviewerName` varchar(35) NOT NULL,
  `location` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviewer`
--

INSERT INTO `reviewer` (`reviewerId`, `reviewerName`, `location`) VALUES
(1, 'Martynas L', 'Vilnius  Lithuania'),
(2, 'RonA_Plymouth', 'Plymouth  United Kingdom'),
(3, 'Stephanie H', 'Nairobi  Kenya'),
(4, 'Peter H', 'Oxford  United Kingdom'),
(5, 'WowLao', 'Vientiane'),
(6, 'tomnjill', 'niagara on the lake  Ontario'),
(7, 'CyrilGubbi', 'Chennai (Madras)  India'),
(8, 'Riyaz M', 'Riyadh  Saudi Arabia'),
(9, 'Melosa81', ''),
(10, 'BeCalAlA', 'Geoje  South Korea');

-- --------------------------------------------------------

--
-- Table structure for table `review_aspect_category`
--

CREATE TABLE `review_aspect_category` (
  `categorySentimentScore` double NOT NULL,
  `categoryId` int(11) NOT NULL,
  `reviewId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review_aspect_category`
--

INSERT INTO `review_aspect_category` (`categorySentimentScore`, `categoryId`, `reviewId`) VALUES
(0, 1, '2fdad21d-9941-46b3-acf7-fbdb3ae5a17'),
(0, 2, '2fdad21d-9941-46b3-acf7-fbdb3ae5a17'),
(3.494999885559082, 3, '2fdad21d-9941-46b3-acf7-fbdb3ae5a17'),
(0, 4, '2fdad21d-9941-46b3-acf7-fbdb3ae5a17'),
(0, 5, '2fdad21d-9941-46b3-acf7-fbdb3ae5a17'),
(0, 1, '6c5369ed-ec78-40c1-ad6a-5f2623cdc76'),
(0, 2, '6c5369ed-ec78-40c1-ad6a-5f2623cdc76'),
(0, 3, '6c5369ed-ec78-40c1-ad6a-5f2623cdc76'),
(0, 4, '6c5369ed-ec78-40c1-ad6a-5f2623cdc76'),
(2.431096315383911, 5, '6c5369ed-ec78-40c1-ad6a-5f2623cdc76'),
(2.9413647651672363, 1, '16e04410-ce59-4390-9cf3-8e7f10feb82'),
(0, 2, '16e04410-ce59-4390-9cf3-8e7f10feb82'),
(0, 3, '16e04410-ce59-4390-9cf3-8e7f10feb82'),
(0, 4, '16e04410-ce59-4390-9cf3-8e7f10feb82'),
(0, 5, '16e04410-ce59-4390-9cf3-8e7f10feb82'),
(0, 1, '3f4eab40-359f-47b3-837d-a0176207528'),
(0, 2, '3f4eab40-359f-47b3-837d-a0176207528'),
(0, 3, '3f4eab40-359f-47b3-837d-a0176207528'),
(0, 4, '3f4eab40-359f-47b3-837d-a0176207528'),
(0, 5, '3f4eab40-359f-47b3-837d-a0176207528'),
(0, 1, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(7.555780410766602, 2, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(7.54764986038208, 3, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 4, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 5, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 1, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 2, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 3, '92c3df0e-b02d-483f-89dd-77252238629'),
(3.4427847862243652, 4, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 5, '92c3df0e-b02d-483f-89dd-77252238629'),
(3.979609251022339, 1, '879f68e1-5711-4192-8445-7856ac9bcac'),
(0, 2, '879f68e1-5711-4192-8445-7856ac9bcac'),
(0, 3, '879f68e1-5711-4192-8445-7856ac9bcac'),
(0, 4, '879f68e1-5711-4192-8445-7856ac9bcac'),
(0, 5, '879f68e1-5711-4192-8445-7856ac9bcac'),
(3.5392847061157227, 1, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 2, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 3, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 4, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 5, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 1, '6a86b7e5-fa15-489b-8751-de75a64512c'),
(0, 2, '6a86b7e5-fa15-489b-8751-de75a64512c'),
(2.414907932281494, 3, '6a86b7e5-fa15-489b-8751-de75a64512c'),
(0, 4, '6a86b7e5-fa15-489b-8751-de75a64512c'),
(0, 5, '6a86b7e5-fa15-489b-8751-de75a64512c'),
(0, 1, 'bac13b39-bf16-49ed-b498-d02ebddbbf7'),
(0, 2, 'bac13b39-bf16-49ed-b498-d02ebddbbf7'),
(0, 3, 'bac13b39-bf16-49ed-b498-d02ebddbbf7'),
(0, 4, 'bac13b39-bf16-49ed-b498-d02ebddbbf7'),
(0, 5, 'bac13b39-bf16-49ed-b498-d02ebddbbf7');

-- --------------------------------------------------------

--
-- Table structure for table `review_aspect_term`
--

CREATE TABLE `review_aspect_term` (
  `aspectTermSentimentScore` double NOT NULL,
  `aspectTermId` int(11) NOT NULL,
  `reviewId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review_aspect_term`
--

INSERT INTO `review_aspect_term` (`aspectTermSentimentScore`, `aspectTermId`, `reviewId`) VALUES
(0, 1, '2fdad21d-9941-46b3-acf7-fbdb3ae5a17'),
(0, 2, '6c5369ed-ec78-40c1-ad6a-5f2623cdc76'),
(0, 3, '6c5369ed-ec78-40c1-ad6a-5f2623cdc76'),
(0, 4, '16e04410-ce59-4390-9cf3-8e7f10feb82'),
(0, 5, '3f4eab40-359f-47b3-837d-a0176207528'),
(0, 6, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 7, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 8, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 9, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 10, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 11, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 12, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 13, '2160fa1a-9818-4163-87ed-3dd5d718fa7'),
(0, 14, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 15, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 16, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 17, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 18, '92c3df0e-b02d-483f-89dd-77252238629'),
(0, 17, '879f68e1-5711-4192-8445-7856ac9bcac'),
(0, 19, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 20, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 21, '0ee30902-3736-4cd3-86b9-dc0e8d74210'),
(0, 22, '6a86b7e5-fa15-489b-8751-de75a64512c'),
(0, 23, 'bac13b39-bf16-49ed-b498-d02ebddbbf7'),
(0, 24, 'bac13b39-bf16-49ed-b498-d02ebddbbf7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aspect_terms`
--
ALTER TABLE `aspect_terms`
  ADD PRIMARY KEY (`aspectTermId`),
  ADD UNIQUE KEY `aspectTerm` (`aspectTerm`);

--
-- Indexes for table `attraction`
--
ALTER TABLE `attraction`
  ADD PRIMARY KEY (`attractionId`),
  ADD UNIQUE KEY `attractionName` (`attractionName`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`reviewId`),
  ADD KEY `attractionId` (`attractionId`),
  ADD KEY `reviewerId` (`reviewerId`);

--
-- Indexes for table `reviewer`
--
ALTER TABLE `reviewer`
  ADD PRIMARY KEY (`reviewerId`);

--
-- Indexes for table `review_aspect_category`
--
ALTER TABLE `review_aspect_category`
  ADD KEY `categoryId` (`categoryId`),
  ADD KEY `reviewId` (`reviewId`);

--
-- Indexes for table `review_aspect_term`
--
ALTER TABLE `review_aspect_term`
  ADD KEY `aspectTermId` (`aspectTermId`),
  ADD KEY `reviewId` (`reviewId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aspect_terms`
--
ALTER TABLE `aspect_terms`
  MODIFY `aspectTermId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `attraction`
--
ALTER TABLE `attraction`
  MODIFY `attractionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reviewer`
--
ALTER TABLE `reviewer`
  MODIFY `reviewerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`attractionId`) REFERENCES `attraction` (`attractionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`reviewerId`) REFERENCES `reviewer` (`reviewerId`);

--
-- Constraints for table `review_aspect_category`
--
ALTER TABLE `review_aspect_category`
  ADD CONSTRAINT `review_aspect_category_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `review_aspect_term`
--
ALTER TABLE `review_aspect_term`
  ADD CONSTRAINT `review_aspect_term_ibfk_1` FOREIGN KEY (`aspectTermId`) REFERENCES `aspect_terms` (`aspectTermId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_aspect_term_ibfk_2` FOREIGN KEY (`reviewId`) REFERENCES `review` (`reviewId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
